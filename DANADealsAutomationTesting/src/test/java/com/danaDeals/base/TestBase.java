package com.danaDeals.base;

import com.danaDeals.utilities.RestUtils;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.log4j.PropertyConfigurator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class TestBase {
    protected static RequestSpecification httpRequest;
    protected static Response response;
    protected String BaseURI = "https://teamdeals.burrow.io";
    protected String responseBody;
    protected org.json.simple.JSONObject requestParams = new org.json.simple.JSONObject();
    protected Logger logger;
    private int idUser;
    private String totalPages;
    private String totalRecords;
    private String currentRecords;
    private String currentPage;
    private int idTransaction;
    private String token;
    private String otp;
    private String pathRegis = "/api/auth/register";
    private String pathRequestOtp = "/api/auth/request-otp";
    private String pathMatchOtp;
    private String pathForgotPassword;
    private String pathLogin = "/api/auth/login";
    private String pathLogout;
    private String pathEditProfile;
    private String pathshowProfile;
    private String time = "90000";
    private String pathTopUp;
    private String pathCreateOrder;
    private String pathTransactionDetail;
    private String pathTransactionHistory;
    private String pathPayOrder;
    private int records;

    public String getPathCreateOrder(String idUser){
        return "/api/user/"+idUser+"/transaction/voucher";
    }
    public String getPathPayOrder(String idUser){
        return "/api/user/"+idUser+"/transaction/voucher";
    }
    public String getPathTransactionDetail(String idUser,String idTransaction){
        return "/api/user/"+idUser+"/transaction/"+idTransaction;
    }
    public String getPathTransactionHistory(String idUser){
        return "/api/user/"+idUser+"/transaction";
    }

    public String getPathTopUp(String idUser){
        return "/api/user/"+idUser+"/transaction/topup";
    }
    public String getPathEditProfile(String idUser) {
        return "/api/user/"+idUser;
    }

    public String getPathshowProfile(String idUser) {
        return  "/api/user/"+idUser;
    }

    public String getPathRegis() {
        return pathRegis;
    }

    public String getPathRequestOtp() {
        return pathRequestOtp;
    }

    public String getPathMatchOtp(String idUser) {
        return "/api/auth/"+idUser+"/match-otp";
    }

    public String getPathForgotPassword(String idUser) {
        return "/api/auth/"+idUser+"/forgot-password";
    }

    public String getPathLogin() {
        return pathLogin;
    }

    public String getPathLogout(String idUser) {
        return "/api/user/"+idUser+"/logout";
    }
    public String getBaseURI(){
        return BaseURI;
    }
    public void setResponseBody(String responseBody) {
        this.responseBody = responseBody;
    }

    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    @BeforeClass
    public void setup() {
        logger=Logger.getLogger("com/danaDeals");
        PropertyConfigurator.configure("Log4j.properties");

        try {
            FileHandler fh = new FileHandler("logs/restAPI.log");
            logger.addHandler(fh);
            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);

        } catch (SecurityException | IOException e) {
            e.printStackTrace();
        }
    }

    public void register(String name, String email, String telephone, String password, String retryPassword){
        RestAssured.baseURI = BaseURI;
        httpRequest = RestAssured.given();

        org.json.simple.JSONObject requestParams  = new org.json.simple.JSONObject();
        requestParams.put("name", name);
        requestParams.put("email", email);
        requestParams.put("phoneNumber", telephone);
        requestParams.put("password", password);
        requestParams.put("confirmPassword", retryPassword);

        httpRequest.header("Content-Type", "application/json");
        httpRequest.body(requestParams.toJSONString());

        response = httpRequest.request(Method.POST, "/api/auth/register");
        responseBody = response.getBody().asString();

        writeFile(name,"name.txt");
        writeFile(email.toLowerCase(),"email.txt");
        writeFile(telephone,"telephone.txt");
        writeFile(password,"password.txt");
    }
    public void login(String telephone, String password){
        RestAssured.baseURI = BaseURI;
        httpRequest = RestAssured.given();

        org.json.simple.JSONObject requestParams = new org.json.simple.JSONObject();
        requestParams.put("phoneNumber", telephone);
        requestParams.put("password", password);

        httpRequest.header("Content-Type", "application/json");
        httpRequest.body(requestParams.toJSONString());

        response = httpRequest.request(Method.POST, "/api/auth/login");
        responseBody = response.getBody().asString();
    }
    public void requestOtp(String telephone){
        RestAssured.baseURI = BaseURI;
        httpRequest = RestAssured.given();

        org.json.simple.JSONObject requestParams = new org.json.simple.JSONObject();
        requestParams.put("phoneNumber", telephone);

        httpRequest.header("Content-Type", "application/json");
        httpRequest.body(requestParams.toJSONString());

        response = httpRequest.request(Method.POST, "/api/auth/request-otp");
        responseBody = response.getBody().asString();
    }

    public void matchOtp(String otp, String idUser){
        RestAssured.baseURI = BaseURI;
        httpRequest = RestAssured.given();

        org.json.simple.JSONObject requestParams = new org.json.simple.JSONObject();
        requestParams.put("otp", otp);

        httpRequest.header("Content-Type", "application/json");
        httpRequest.body(requestParams.toJSONString());

        response = httpRequest.request(Method.POST, "/api/auth/"+idUser+"/match-otp");
        responseBody = response.getBody().asString();
    }
    public void logout(String idUser,String token){
        RestAssured.baseURI = BaseURI;
        httpRequest = RestAssured.given();

        if(!token.equals("empty"))
            httpRequest.header("Authorization", "Bearer " + token);

        org.json.simple.JSONObject requestParams = new org.json.simple.JSONObject();
        httpRequest.header("Content-Type", "application/json");
        httpRequest.body(requestParams.toJSONString());

        response = httpRequest.request(Method.POST, "/api/user/"+idUser+"/logout");
        responseBody = response.getBody().asString();
    }
    public void forgotPassword(String password, String confirmPassword,String idUser){
        RestAssured.baseURI = BaseURI;
        httpRequest = RestAssured.given();

        org.json.simple.JSONObject requestParams = new org.json.simple.JSONObject();
        requestParams.put("password", password);
        requestParams.put("confirmPassword", confirmPassword);

        httpRequest.header("Content-Type", "application/json");
        httpRequest.body(requestParams.toJSONString());

        response = httpRequest.request(Method.POST, "/api/auth/"+idUser+"/forgot-password");
        responseBody = response.getBody().asString();
    }

    public void showProfile(String idUser, String token){
        RestAssured.baseURI = BaseURI;
        httpRequest = RestAssured.given();

        httpRequest.header("Authorization", "Bearer " + token);
        httpRequest.header("Content-Type", "application/json");
        httpRequest.body(requestParams.toJSONString());

        response = httpRequest.request(Method.GET, "/api/user/"+idUser);
        responseBody = response.getBody().asString();
    }

    public void editUser(String name, String email, String oldPassword, String newPassword, String confirmPassword, String id, String token){
        RestAssured.baseURI = BaseURI;
        httpRequest = RestAssured.given();

        org.json.simple.JSONObject requestParams = new org.json.simple.JSONObject();

        httpRequest.header("Authorization", "Bearer " + token);

        requestParams = new org.json.simple.JSONObject();

        if(!name.equals("empty")) {
            requestParams.put("name", name);
        }
        if(!email.equals("empty")) {
            requestParams.put("email", email);
        }
        if(!oldPassword.equals("empty")) {
            requestParams.put("oldPassword", oldPassword);
        }
        if(!newPassword.equals("empty")) {
            requestParams.put("newPassword", newPassword);
        }
        if(!confirmPassword.equals("empty")) {
            requestParams.put("confirmPassword", confirmPassword);
        }

        httpRequest.header("Content-Type", "application/json");
        httpRequest.body(requestParams.toJSONString());

        response = httpRequest.request(Method.PUT, "/api/user/"+id);
        responseBody = response.getBody().asString();
    }

    public void createOrderVoucher(String idVoucher, String idUser, String token){
        RestAssured.baseURI = BaseURI;
        httpRequest = RestAssured.given();

        org.json.simple.JSONObject requestParams = new org.json.simple.JSONObject();

        httpRequest.header("Authorization", "Bearer " + token);
        requestParams.put("idVoucher", Integer.parseInt(idVoucher));

        httpRequest.header("Content-Type", "application/json");
        httpRequest.body(requestParams.toJSONString());

        response = httpRequest.request(Method.POST, "/api/user/"+idUser+"/transaction/voucher");
        responseBody = response.getBody().asString();
    }

    public void payOrderVoucher(String idTransaction, String idUser, String token){
        RestAssured.baseURI = BaseURI;
        httpRequest = RestAssured.given();

        org.json.simple.JSONObject requestParams = new org.json.simple.JSONObject();

        httpRequest.header("Authorization", "Bearer " + token);
        requestParams.put("idTransaction", Integer.parseInt(idTransaction));

        httpRequest.header("Content-Type", "application/json");
        httpRequest.body(requestParams.toJSONString());

        response = httpRequest.request(Method.PUT, "/api/user/"+idUser+"/transaction/voucher");
        responseBody = response.getBody().asString();
    }

    public void topUp(String virtualNumber, String amount, String idUser, String token){
        RestAssured.baseURI = BaseURI;
        httpRequest = RestAssured.given();

        org.json.simple.JSONObject requestParams = new org.json.simple.JSONObject();

        httpRequest.header("Authorization", "Bearer " + token);
        requestParams.put("virtualNumber", virtualNumber);
        requestParams.put("amount", Double.parseDouble(amount));

        httpRequest.header("Content-Type", "application/json");
        httpRequest.body(requestParams.toJSONString());

        response = httpRequest.request(Method.POST, "/api/user/"+idUser+"/transaction/topup");
        responseBody = response.getBody().asString();
    }

    public void transactionHistory(String category, String start_date, String end_date, String page, String idUser, String token)
    {
        int i = 0;
        String link = "/api/user/"+idUser+"/transaction";
        RestAssured.baseURI = BaseURI;
        httpRequest = RestAssured.given();

        httpRequest.header("Authorization", "Bearer " + token);
        httpRequest.header("Content-Type", "application/json");
        httpRequest.body(requestParams.toJSONString());
        if (!category.equals("empty"))
        {
            link+="?category="+category;
            i++;
        }
        if (!start_date.equals("empty"))
        {
            if(i==0) {
                link += "?";
                i++;
            }
            else
                link+="&";
            link +="filter-start-date="+start_date;
        }
        if (!end_date.equals("empty"))
        {
            if(i==0)
            {
                link+="?";
                i++;
            }
            else
                link+="&";
            link +="filter-end-date="+end_date;
        }
        if (!page.equals("empty"))
        {
            if(i==0){
                link+="?";
                i++;
            }
            else
                link+="&";
            link +="page="+page;
        }
        logger.info("link : "+link);
        response = httpRequest.request(Method.GET, link);
        responseBody = response.getBody().asString();
    }

    public void transactionDetail(String idTransaction, String idUser, String token)
    {
        RestAssured.baseURI = BaseURI;
        httpRequest = RestAssured.given();

        httpRequest.header("Authorization", "Bearer " + token);
        httpRequest.header("Content-Type", "application/json");
        httpRequest.body(requestParams.toJSONString());

        response = httpRequest.request(Method.GET, "/api/user/"+idUser+"/transaction/"+idTransaction);
        responseBody = response.getBody().asString();
    }

    public void createVoucher(String voucherName, String voucherPrice, String discount, String maxDiscount,
                              String quota, String expiredDate, String status, String idUser, String idMerchant,
                              String token)
    {
        RestAssured.baseURI = BaseURI;
        httpRequest = RestAssured.given();

        httpRequest.header("Authorization", "Bearer " + token);
        httpRequest.header("Content-Type", "application/json");

        org.json.simple.JSONObject requestParams = new org.json.simple.JSONObject();
        requestParams.put("voucherName", voucherName);
        requestParams.put("voucherPrice", Double.parseDouble(voucherPrice));
        requestParams.put("discount", Double.parseDouble(discount));
        requestParams.put("maxDiscount", Double.parseDouble(maxDiscount));
        requestParams.put("quota", Integer.parseInt(quota));
        requestParams.put("expiredDate", expiredDate);
        requestParams.put("status", status);

        httpRequest.body(requestParams.toJSONString());

        response = httpRequest.request(Method.POST, "/api/admin/"+idUser+"/merchant/"+idMerchant+"/vouchers");
        responseBody = response.getBody().asString();
    }

    public void detailVoucher(String idVoucher, String token)
    {
        RestAssured.baseURI = BaseURI;
        httpRequest = RestAssured.given();

        httpRequest.header("Authorization", "Bearer " + token);
        response = httpRequest.request(Method.GET, "/api/admin/voucher-detail-voucher/"+idVoucher);
        responseBody = response.getBody().asString();
    }

    //whatToDo = 1 --> change status, whatToDo = 2 --> restock
    public void updateVoucher(int whatToDo, String status, String quota, String idVoucher, String token)
    {

        RestAssured.baseURI = BaseURI;
        httpRequest = RestAssured.given();

        httpRequest.header("Authorization", "Bearer "+token);
        httpRequest.header("Content-Type", "application/json");

        org.json.simple.JSONObject requestParams = new org.json.simple.JSONObject();

        if (whatToDo == 1) {

            requestParams.put("status", status);

        } else if (whatToDo == 2) {

            requestParams.put("status", status);
            requestParams.put("quota", Integer.parseInt(quota));

        } else {
            logger.info("Wrong choice at update voucher");
        }

        httpRequest.body(requestParams.toJSONString());
        response = httpRequest.request(Method.PUT, "/api/admin/update-status-voucher/"+idVoucher+"/restock");
        responseBody = response.getBody().asString();
    }

    public void setIdUser() throws JSONException {
        JsonPath jsonPath = response.jsonPath();
        String data = jsonPath.get("data.user.idUser").toString();
        logger.info("idUser  ===> "+data);
        this.idUser = Integer.parseInt(data);
    }
    public void setIdUserReqOtp() throws JSONException {
        JsonPath jsonPath = response.jsonPath();
        String data = jsonPath.get("data.idUser").toString();
        logger.info("idUser  ===> "+data);
        this.idUser = Integer.parseInt(data);
    }
    public void setIdTransaction() throws JSONException {
        JsonPath jsonPath = response.jsonPath();
        String data = jsonPath.get("data").toString();
        JSONObject jsonObject = new JSONObject(data);
        int idTransaction = jsonObject.getInt("idTransaction");
        logger.info("idTransaction  ===> "+idTransaction);
        this.idTransaction = idTransaction;
    }
    public int getIdTransaction(){
        return this.idTransaction;
    }
    public void set() throws JSONException {
        JsonPath jsonPath = response.jsonPath();
        String data = jsonPath.get("data").toString();
        JSONObject jsonObject = new JSONObject(data);
        int idUser = jsonObject.getInt("idUser");
        logger.info("idUser  ===>"+idUser);
        this.idUser = idUser;
    }

    public int getIdUser(){
        return idUser;
    }

    public String getToken(){
        return this.token;
    }

    public void setToken() throws JSONException {
        JsonPath jsonPath = response.jsonPath();
        String data = jsonPath.get("data.token").toString();
        logger.info("token  ===>"+data);
        this.token = data;
    }

    public void checkBody(String body) {
        logger.info("***** Check Message *****");
        logger.info("Expected result: "+body);

        JsonPath jsonPath = response.jsonPath();
        String message = jsonPath.get("message").toString();
        logger.info("Actual result: "+message);
        Assert.assertTrue(message.equals(body));
    }

    public void checkStatusCode(String sc) {
        logger.info("***** Check Status Code *****");
        logger.info("Expected result: "+sc);

        int statusCode = response.getStatusCode();
        logger.info("Actual result: " + statusCode);
        Assert.assertEquals(statusCode, Integer.parseInt(sc));
    }

    public void checkDevStatus(String status){
        logger.info("***** Check Developer Status *****");
        logger.info("Expected result: "+status);

        JsonPath jsonPath = response.jsonPath();
        String data = jsonPath.get("status").toString();
        logger.info("dev status ===> "+data);
        Assert.assertEquals(data,status);
    }
    public void checkResponseTime(String rt) {
        logger.info("***** Check Response Time *****");

        long responseTime = response.getTime();
        logger.info("Response Time = " + responseTime);
        Assert.assertTrue(responseTime<Long.parseLong(this.time));
    }
    public void checkTimestamp(){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
        LocalDateTime now = LocalDateTime.now();

        String currentDate = dtf.format(now);

        JsonPath jsonPath = response.jsonPath();
        String data = jsonPath.get("timestamp").toString();
        logger.info("timestamp ===> "+data);
        Assert.assertTrue(data.contains(currentDate));
    }
    public void checkPath(String path){
        logger.info("***** Check Path *****");
        logger.info("Expected result: "+path);

        JsonPath jsonPath = response.jsonPath();
        String data = jsonPath.get("path").toString();
        logger.info("Actual result:  "+data);
        Assert.assertEquals(path,data);
    }

    public int getNumberOfVoucher() {
        JsonPath jsonPath = response.jsonPath();
        int size = jsonPath.get("data[0].numberOfElements");
        return size;
    }

    public void checkVoucherInAPage(int totalVoucherInAPage) {
        logger.info("***** Check Total Voucher In A Page *****");
        logger.info("Expected result: 10");
        logger.info("Actual result: "+totalVoucherInAPage);

        Assert.assertTrue(totalVoucherInAPage <= 10);
    }

    public void checkDataEquals(String get, String dataTest) {
        logger.info("***** Check Data Content *****");
        logger.info("Expected result: "+dataTest);

        JsonPath jsonPath = response.jsonPath();
        String respData = jsonPath.get(get).toString();
        logger.info("Actual result: "+respData);

        Assert.assertTrue(respData.equals(dataTest));
    }
    public void checkRegisData(String name, String email, String phoneNumber, String idUser)
    {
        logger.info("***** Check Register Data Content *****");
        JsonPath jsonPath = response.jsonPath();
        String respName = jsonPath.get("data.user.name").toString();
        String respEmail = jsonPath.get("data.user.email").toString();
        String respPhoneNumber = jsonPath.get("data.user.phoneNumber").toString();
        String respIdUser= jsonPath.get("data.user.idUser").toString();
        String respBalance= jsonPath.get("data.user.balance").toString();
        String respIdRole= jsonPath.get("data.user.idRole").toString();
        String respRoleName = jsonPath.get("data.user.roleName").toString();

        logger.info("Actual name: "+respName);
        logger.info("Expect name: "+name);
        Assert.assertEquals(respName,name);
        logger.info("Actual email: "+respEmail);
        logger.info("Expect email: "+email.toLowerCase());
        Assert.assertEquals(respEmail,email.toLowerCase());
        logger.info("Actual phone number: "+respPhoneNumber);
        logger.info("Expect phone number: "+RestUtils.changeTelephone(phoneNumber) + phoneNumber);
        if(RestUtils.changeTelephone(phoneNumber).equals(respPhoneNumber))
            Assert.assertTrue(true);
        else if(phoneNumber.equals(respPhoneNumber))
            Assert.assertTrue(true);
        else
            Assert.assertTrue(false);
        //Assert.assertEquals(respPhoneNumber,phoneNumber);
        logger.info("Actual id role: "+respIdRole);
        logger.info("Expect id role: 2");
        Assert.assertEquals(respIdRole,"2");
        logger.info("Actual id user: "+respIdUser);
        logger.info("Expect id user: "+idUser);
        Assert.assertEquals(respIdUser,idUser);
        logger.info("Actual balance: "+respBalance);
        logger.info("Expect balance: 0.0");
        Assert.assertEquals(respBalance,"0.0");
        logger.info("Actual role name: "+respRoleName);
        logger.info("Expect role name: User");
        Assert.assertEquals(respRoleName,"User");
        writeFile("0.0","balance.txt");
    }
    public void checkLoginData(String phoneNumber, String idUser)
    {
        logger.info("***** Check Login Data Content *****");
        JsonPath jsonPath = response.jsonPath();

        String name = loadFile("name.txt");
        String email = loadFile("email.txt");
        String balance = loadFile("balance.txt");
        String respName = jsonPath.get("data.user.name").toString();
        String respEmail = jsonPath.get("data.user.email").toString();
        String respPhoneNumber = jsonPath.get("data.user.phoneNumber").toString();
        String respIdUser= jsonPath.get("data.user.idUser").toString();
        String respBalance= jsonPath.get("data.user.balance").toString();
        String respIdRole= jsonPath.get("data.user.idRole").toString();
        String respRoleName = jsonPath.get("data.user.roleName").toString();

        logger.info("Actual name: "+respName);
        logger.info("Expect name: "+name);
        Assert.assertEquals(respName,name);
        logger.info("Actual email: "+respEmail);
        logger.info("Expect email: "+email.toLowerCase());
        Assert.assertEquals(respEmail,email.toLowerCase());
        logger.info("Actual phone number: "+respPhoneNumber);
        logger.info("Expect phone number: "+phoneNumber);
        if(RestUtils.changeTelephone(phoneNumber).equals(respPhoneNumber))
            Assert.assertTrue(true);
        else if(phoneNumber.equals(respPhoneNumber))
            Assert.assertTrue(true);
        else
            Assert.assertTrue(false);
        //Assert.assertEquals(respPhoneNumber,phoneNumber);
        logger.info("Actual id role: "+respIdRole);
        logger.info("Expect id role: 2");
        Assert.assertEquals(respIdRole,"2");
        logger.info("Actual id user: "+respIdUser);
        logger.info("Expect id user: "+idUser);
        Assert.assertEquals(respIdUser,idUser);
        logger.info("Actual balance: "+respBalance);
        logger.info("Expect balance: "+balance);
        Assert.assertEquals(respBalance,balance);
        logger.info("Actual role name: "+respRoleName);
        logger.info("Expect role name: User");
        Assert.assertEquals(respRoleName,"User");
    }

    public void checkShowProfileData()
    {
        String idUser = loadFile("idUser.txt");
        String phoneNumber = loadFile("telephone.txt");
        String name = loadFile("name.txt");
        String email = loadFile("email.txt");

        logger.info("***** Check Show Profile Data Content *****");
        JsonPath jsonPath = response.jsonPath();

        String respName = jsonPath.get("data.name").toString();
        String respEmail = jsonPath.get("data.email").toString();
        String respPhoneNumber = jsonPath.get("data.phoneNumber").toString();
        String respIdUser= jsonPath.get("data.idUser").toString();

        logger.info("Actual name: "+respName);
        logger.info("Expect name: "+name);
        Assert.assertEquals(respName,name);
        logger.info("Actual email: "+respEmail);
        logger.info("Expect email: "+email.toLowerCase());
        Assert.assertEquals(respEmail,email.toLowerCase());
        logger.info("Actual phone number: "+respPhoneNumber);
        logger.info("Expect phone number: "+phoneNumber);
        if(RestUtils.changeTelephone(phoneNumber).equals(respPhoneNumber))
            Assert.assertTrue(true);
        else if(phoneNumber.equals(respPhoneNumber))
            Assert.assertTrue(true);
        else
            Assert.assertTrue(false);
        //Assert.assertEquals(respPhoneNumber,phoneNumber);
        logger.info("Actual id user: "+respIdUser);
        logger.info("Expect id user: "+idUser);
        Assert.assertEquals(respIdUser,idUser);

    }
    public void checkRequestOtp(String idUser){
        Assert.assertEquals(idUser,loadFile("idUser.txt"));
    }
    public void checkEmptyData(){
        JsonPath jsonPath = response.jsonPath();
        Assert.assertEquals((boolean[]) jsonPath.get("data"),null);
    }
    public void checkDataContains(String get, String dataTest) {
        logger.info("***** Check Data Content *****");
        logger.info("Expected result: "+dataTest);

        JsonPath jsonPath = response.jsonPath();
        String respData = jsonPath.get(get).toString();
        logger.info("Actual result: "+respData);

        Assert.assertTrue(respData.contains(dataTest));
    }

    //choice = 1 for discount, choice = 2 for voucher price
    public void checkSortingVoucher(int choice) {
        logger.info("***** Check Sorting Voucher *****");

        JsonPath jsonPath = response.jsonPath();

        switch (choice) {
            case 1:
                float discount1 = jsonPath.get("data.content.discount[0][0]");
                float discount2 = jsonPath.get("data.content.discount[0][1]");
                Assert.assertTrue(discount1 >= discount2);
                break;
            case 2:
                float price1 = jsonPath.get("data.content.voucherPrice[0][0]");
                float price2 = jsonPath.get("data.content.voucherPrice[0][1]");
                Assert.assertTrue(price1 >= price2);
                break;
            default:
                break;
        }

    }

    public String loadFile(String nameFile){
        String content = "";
        try {
            FileReader fr = new FileReader("File/"+nameFile);
            BufferedReader br = new BufferedReader(fr);

            content = br.readLine();
            br.close();
            fr.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return content;
    }
    public void checkEditProfileData(String name, String email,String password){
        logger.info("***** Check Edit Profile Data Content *****");
        JsonPath jsonPath = response.jsonPath();
        String idUser = loadFile("idUser.txt");
        if(name.equals("empty")) {
            name = loadFile("name.txt");
        }
        if(email.equals("empty")) {
            email = loadFile("email.txt");
        }
        String phoneNumber = loadFile("telephone.txt");
        String respName = jsonPath.get("data.name").toString();
        String respEmail = jsonPath.get("data.email").toString();
        String respPhoneNumber = jsonPath.get("data.phoneNumber").toString();
        String respIdUser= jsonPath.get("data.idUser").toString();

        logger.info("Actual name: "+respName);
        logger.info("Expect name: "+name);
        Assert.assertEquals(respName,name);
        logger.info("Actual email: "+respEmail);
        logger.info("Expect email: "+email.toLowerCase());
        Assert.assertEquals(respEmail,email.toLowerCase());
        logger.info("Actual phone number: "+respPhoneNumber);
        logger.info("Expect phone number: "+phoneNumber);
        if(RestUtils.changeTelephone(phoneNumber).equals(respPhoneNumber))
            Assert.assertTrue(true);
        else if(phoneNumber.equals(respPhoneNumber))
            Assert.assertTrue(true);
        else
            Assert.assertTrue(false);
        //Assert.assertEquals(respPhoneNumber,phoneNumber);
        logger.info("Actual id user: "+respIdUser);
        logger.info("Expect id user: "+idUser);
        Assert.assertEquals(respIdUser,idUser);
        if(!name.equals("empty"))
        {
            writeFile(name,"name.txt");
        }
        if(!email.equals("empty"))
        {
            writeFile(email,"email.txt");
        }
        if(!password.equals("empty"))
        {
            writeFile(password,"password.txt");
        }

    }
    public void setRecords(){
        JsonPath jsonPath = response.jsonPath();
        this.records = Integer.parseInt(jsonPath.get("data.pagination.totalRecords").toString());
    }
    public int getRecords(){
        return this.records;
    }
    public void checkTransactionHistory(String category, String startDate, String endDate, String page,String records) throws ParseException {
        logger.info("***** Check Transaction History Content *****");
        logger.info("Records = "+getRecords());
        JsonPath jsonPath = response.jsonPath();
        SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd");
        Date start = sdformat.parse(startDate);
        Date end = sdformat.parse(endDate);
        Date temp = null;
        if(page.equals("empty")){
            page="0";
        }
        if(Integer.parseInt(page)==Math.ceil(Double.parseDouble(records)/10.0)) {
            //case if last page
            if(jsonPath.get("data.content["+Integer.toString(Integer.parseInt(records)+1)+"]")!=null){
                Assert.assertTrue(false);
            }
        }
        else if(Integer.parseInt(page)<Math.floor(Double.parseDouble(records)/10.0))
        {
            //case if not last page
            if(jsonPath.get("data.content[10]")!=null){
                Assert.assertTrue(false);
            }
        }

        for(int i=0;i<Integer.parseInt(records);i++){
            logger.info("Transaction - "+jsonPath.get("data.content["+i+"].idTransaction")+" status = "+jsonPath.get("data.content["+i+"].transactionStatus").toString());
            if(category.equals("COMPLETED")) {
                if ((!jsonPath.get("data.content[" + i + "].transactionStatus").toString().equals("Failed")) && (!jsonPath.get("data.content[" + i + "].transactionStatus").toString().equals("Success")) && (!jsonPath.get("data.content[" + i + "].transactionStatus").toString().equals("Refund"))){
                    Assert.assertTrue(false);
                }
            }
            else if (category.equals("IN-PROGRESS")){
                if ((!jsonPath.get("data.content[" + i + "].transactionStatus").toString().equals("In progress"))){
                    Assert.assertTrue(false);
                }
            }
            else{
                if ((!jsonPath.get("data.content[" + i + "].transactionStatus").toString().equals("Failed")) && (!jsonPath.get("data.content[" + i + "].transactionStatus").toString().equals("Success")) && (!jsonPath.get("data.content[" + i + "].transactionStatus").toString().equals("Refund"))&&(!jsonPath.get("data.content[" + i + "].transactionStatus").toString().equals("In progress"))){
                    Assert.assertTrue(false);
                }
            }
            Date date = sdformat.parse(jsonPath.get("data.content["+i+"].transactionDate").toString());
            logger.info("Transaction - "+jsonPath.get("data.content["+i+"].idTransaction")+" date = "+jsonPath.get("data.content["+i+"].transactionDate").toString());

            if((start.compareTo(date)>0)||(end.compareTo(date)<0)){
                Assert.assertTrue(false);
            }
            if((i>0)&&(date.compareTo(temp)>0)){
                Assert.assertTrue(false);
            }
            temp = date;
            logger.info("Transaction - "+jsonPath.get("data.content["+i+"].idTransaction")+" amount = "+jsonPath.getDouble("data.content["+i+"].amount"));
        }
        logger.info("Current page : "+jsonPath.get("data.pagination.currentPage").toString());
        Assert.assertEquals(jsonPath.get("data.pagination.currentPage").toString(),page);
        logger.info("Total pages : "+jsonPath.get("data.pagination.totalPages").toString());
        logger.info("Current records  : "+jsonPath.get("data.pagination.currentRecords").toString());
        Assert.assertTrue((jsonPath.getInt("data.pagination.currentRecords")<11)&&(jsonPath.getInt("data.pagination.currentRecords")>0));
        logger.info("Total records : "+jsonPath.get("data.pagination.totalRecords").toString());

    }
    public void checkEmptyTransactionHistory(){
        logger.info("***** Check Transaction History Content *****");
        logger.info("Records = "+getRecords());
        JsonPath jsonPath = response.jsonPath();
        if(jsonPath.get("data.content[0]")!=null){
            Assert.assertTrue(false);
        }
    }

    public void checkTransactionDetail(String idTransaction,String status){
        logger.info("***** Check Transaction Detail Content *****");
        JsonPath jsonPath = response.jsonPath();
        logger.info("Id Transaction = "+jsonPath.get("data.idTransaction").toString());
        logger.info("Service Name = "+jsonPath.get("data.serviceName").toString());
        logger.info("Amount = "+jsonPath.getDouble("data.amount"));
        logger.info("Is credit = "+jsonPath.get("data.isCredit"));
        logger.info("Transaction Date = "+jsonPath.get("data.transactionDate"));
        logger.info("Id Transaction Status = "+jsonPath.get("data.idTransactionStatus"));
        logger.info("Voucher information");
        logger.info("Service Name = "+jsonPath.get("data.serviceName"));
        Assert.assertEquals(jsonPath.get("data.idTransaction").toString(),idTransaction);
        if(!status.equals("empty")){
            Assert.assertEquals(jsonPath.get("data.transactionStatus"),status);
        }
    }
    public void writeFile(String token, String nameFile) {
        try {
            FileWriter fw = new FileWriter("File/" + nameFile);
            BufferedWriter bw = new BufferedWriter(fw);

            bw.write(token);
            bw.flush();
            bw.close();
            fw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

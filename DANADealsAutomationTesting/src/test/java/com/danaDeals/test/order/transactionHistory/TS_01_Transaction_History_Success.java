package com.danaDeals.test.order.transactionHistory;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.Utility;
import org.json.JSONException;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class TS_01_Transaction_History_Success extends TestBase {
    @BeforeClass
    private void getTransactionHistory() throws InterruptedException {
        logger.info("********" + getClass().getName() + "*********");
        Thread.sleep(5);
    }

    @Test(dataProvider = "transactionHistorySuccess", priority = 0)
    private void getTransactionHistorySuccess(String category, String page) throws JSONException, ParseException {
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDateTime now = LocalDateTime.now();

        String startDate = dtf.format(now);
        String endDate = dtf.format(now);

        transactionHistory(category,startDate,endDate,page,idUser,token);

        //checking response body
        checkBody("Transaction history are successfully collected.");

        //checking status code
        checkDevStatus("037");
        checkStatusCode("200");

        //check timestamp
        checkTimestamp();

        //check path
        checkPath(getPathTransactionHistory(idUser));

        //check response data
        setRecords();
        if((page.equals("empty"))||page.equals("0"))
        {
            checkTransactionHistory(category, startDate, endDate, page, Integer.toString(getRecords()));
}
        else {
            checkEmptyTransactionHistory();
        }

        //checking response time
        checkResponseTime("3000");
    }


    @DataProvider(name="transactionHistorySuccess")
    private Object[][] getDataEmpty() throws IOException {
        String path = "src/test/java/com/danaDeals/test/order/transactionHistory/transactionHistorySuccess.xlsx";
        int rownum = Utility.getRowCount(path, "Sheet1");
        int colcount = Utility.getCellCount(path, "Sheet1",1);
        String dataEmployes [][] = new String[rownum][colcount];
        for(int i=1;i<=rownum;i++) {
            for(int j=0;j<colcount;j++) {
                dataEmployes[i-1][j] = Utility.getCellData(path,"Sheet1",i,j);
            }
        }
        return(dataEmployes);
    }

}

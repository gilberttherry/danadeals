package com.danaDeals.test.order.transcationDetail;

import com.danaDeals.base.TestBase;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TS_02_Transcation_Detail_Failed extends TestBase {

    @BeforeClass
    private void getTransactionDetail() throws InterruptedException {
        logger.info("********" + getClass().getName() + "*********");
        Thread.sleep(5);
    }

    @Test(priority = 0)
    private void transactionDetailInvalidIdTransaction(){
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");
        String idTransaction = "1";

        transactionDetail(idTransaction,idUser,token);

        //check response body
        checkBody("Transaction is not found.");

        //check status
        checkDevStatus("032");
        checkStatusCode("404");

        //check timestamp
        checkTimestamp();

        //check path
        checkPath(getPathTransactionDetail(idUser,idTransaction));

        //check response data
        checkEmptyData();

        //check response time
        checkResponseTime("3000");

    }

    @Test(priority = 1)
    private void transactionDetailInvalidIdUser(){
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");
        String idTransaction = loadFile("idTransaction.txt");
        for(int i=0;i<4;i++) {
            if(i==0){
                idUser=Integer.toString(Integer.parseInt(idUser) + 1);
            }
            else if(i==1){
                token=token.toUpperCase();
            }
            else if(i==2){
                token=token.toLowerCase();
            }
            else{
                token=loadFile("oldToken.txt");
            }

            transactionDetail(idTransaction, idUser, token);

            //check response body
            checkBody("You are not authorized.");

            //check status
            checkDevStatus("021");
            checkStatusCode("401");

            //check timestamp
            checkTimestamp();

            //check path
            checkPath(getPathTransactionDetail(idUser, idTransaction));

            //check response data
            checkEmptyData();

            //check response time
            checkResponseTime("3000");
        }
    }

}

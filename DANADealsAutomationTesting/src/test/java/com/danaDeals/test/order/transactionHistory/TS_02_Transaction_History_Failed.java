package com.danaDeals.test.order.transactionHistory;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.Utility;
import org.json.JSONException;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class TS_02_Transaction_History_Failed extends TestBase {

    @BeforeClass
    private void getTransactionHistory() throws InterruptedException {
        logger.info("********" + getClass().getName() + "*********");
        Thread.sleep(5);
    }

    @Test(dataProvider = "transactionHistoryCategoryError", priority = 0)
    private void getTransactionHistoryCategoryError(String category, String startDate, String endDate, String page) throws JSONException {
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        if(startDate.equals("valid"))
        {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
            LocalDateTime now = LocalDateTime.now();
            startDate = dtf.format(now);
        }

        if(endDate.equals("valid"))
        {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
            LocalDateTime now = LocalDateTime.now();
            endDate = dtf.format(now);
        }

        transactionHistory(category,startDate,endDate,page,idUser,token);

        //checking response body
        checkBody("Category only consists of COMPLETED and  IN-PROGRESS content.");

        //checking status code
        checkDevStatus("074");
        checkStatusCode("400");

        //check timestamp
        checkTimestamp();

        //check path
        checkPath(getPathTransactionHistory(idUser));

        //check response data
        checkEmptyData();

        //checking response time
        checkResponseTime("3000");
    }


    @DataProvider(name="transactionHistoryCategoryError")
    private Object[][] getDataEmptyCategoryError() throws IOException {
        String path = "src/test/java/com/danaDeals/test/order/transactionHistory/transactionHistoryCategoryError.xlsx";
        int rownum = Utility.getRowCount(path, "Sheet1");
        int colcount = Utility.getCellCount(path, "Sheet1",1);
        String dataEmployes [][] = new String[rownum][colcount];
        for(int i=1;i<=rownum;i++) {
            for(int j=0;j<colcount;j++) {
                dataEmployes[i-1][j] = Utility.getCellData(path,"Sheet1",i,j);
            }
        }
        return(dataEmployes);
    }

    @Test(dataProvider = "transactionHistoryDateInvalidError", priority = 1)
    private void getTransactionHistoryDateInvalidError(String category, String startDate, String endDate, String page) throws JSONException {
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        if(startDate.equals("valid"))
        {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
            LocalDateTime now = LocalDateTime.now();
            startDate = dtf.format(now);
        }

        if(endDate.equals("valid"))
        {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
            LocalDateTime now = LocalDateTime.now();
            endDate = dtf.format(now);
        }

        transactionHistory(category,startDate,endDate,page,idUser,token);

        //checking response body
        checkBody("Please use a valid date format (YYYY-mm-dd).");

        //checking status code
        checkDevStatus("077");
        checkStatusCode("400");

        //check timestamp
        checkTimestamp();

        //check path
        checkPath(getPathTransactionHistory(idUser));

        //check response data
        checkEmptyData();

        //checking response time
        checkResponseTime("3000");
    }


    @DataProvider(name="transactionHistoryDateInvalidError")
    private Object[][] getDataEmptyDateInvalidError() throws IOException {
        String path = "src/test/java/com/danaDeals/test/order/transactionHistory/transactionHistoryDateInvalidError.xlsx";
        int rownum = Utility.getRowCount(path, "Sheet1");
        int colcount = Utility.getCellCount(path, "Sheet1",1);
        String dataEmployes [][] = new String[rownum][colcount];
        for(int i=1;i<=rownum;i++) {
            for(int j=0;j<colcount;j++) {
                dataEmployes[i-1][j] = Utility.getCellData(path,"Sheet1",i,j);
            }
        }
        return(dataEmployes);
    }

    @Test(priority = 2)
    private void transactionHistoryNegativePage(){
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        String category="COMPLETED";
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDateTime now = LocalDateTime.now();
        String endDate = dtf.format(now);
        String startDate = dtf.format(now);
        String page = "-1";
        transactionHistory(category,startDate,endDate,page,idUser,token);
        //checking response body
        checkBody("A page cannot have pagination numbers less than 0.");

        //checking status code
        checkDevStatus("076");
        checkStatusCode("400");

        //check timestamp
        checkTimestamp();

        //check path
        checkPath(getPathTransactionHistory(idUser));

        //check response data
        checkEmptyData();

        //checking response time
        checkResponseTime("3000");
    }

    @Test(dataProvider = "transactionHistoryMoreThan7Days", priority = 3)
    private void getTransactionHistoryMoreThan7Days(String category, String startDate, String endDate, String page) throws JSONException {
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        if(startDate.equals("valid"))
        {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
            LocalDateTime now = LocalDateTime.now();
            startDate = dtf.format(now);
        }

        if(endDate.equals("valid"))
        {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
            LocalDateTime now = LocalDateTime.now();
            endDate = dtf.format(now);
        }

        transactionHistory(category,startDate,endDate,page,idUser,token);


        //checking response body
        checkBody("Maximum search per history is within 7 days.");

        //checking status code
        checkDevStatus("079");
        checkStatusCode("400");

        //check timestamp
        checkTimestamp();

        //check path
        checkPath(getPathTransactionHistory(idUser));

        //check response data
        checkEmptyData();

        //checking response time
        checkResponseTime("3000");
    }


    @DataProvider(name="transactionHistoryMoreThan7Days")
    private Object[][] getDataEmptyMoreThan7Days() throws IOException {
        String path = "src/test/java/com/danaDeals/test/order/transactionHistory/transactionHistoryMoreThan7Days.xlsx";
        int rownum = Utility.getRowCount(path, "Sheet1");
        int colcount = Utility.getCellCount(path, "Sheet1",1);
        String dataEmployes [][] = new String[rownum][colcount];
        for(int i=1;i<=rownum;i++) {
            for(int j=0;j<colcount;j++) {
                dataEmployes[i-1][j] = Utility.getCellData(path,"Sheet1",i,j);
            }
        }
        return(dataEmployes);
    }

    @Test(dataProvider = "transactionHistoryPageError1", priority = 4)
    private void getTransactionHistoryPageError(String category, String startDate, String endDate, String page) throws JSONException {
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        if(startDate.equals("valid"))
        {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
            LocalDateTime now = LocalDateTime.now();
            startDate = dtf.format(now);
        }

        if(endDate.equals("valid"))
        {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
            LocalDateTime now = LocalDateTime.now();
            endDate = dtf.format(now);
        }

        transactionHistory(category,startDate,endDate,page,idUser,token);

        //checking response body
        checkBody("Page parameter uses only numbers.");

        //checking status code
        checkDevStatus("075");
        checkStatusCode("400");

        //check timestamp
        checkTimestamp();

        //check path
        checkPath(getPathTransactionHistory(idUser));

        //check response data
        checkEmptyData();

        //checking response time
        checkResponseTime("3000");
    }


    @DataProvider(name="transactionHistoryPageError1")
    private Object[][] getDataEmptyPageError() throws IOException {
        String path = "src/test/java/com/danaDeals/test/order/transactionHistory/transactionHistoryPageError1.xlsx";
        int rownum = Utility.getRowCount(path, "Sheet1");
        int colcount = Utility.getCellCount(path, "Sheet1",1);
        String dataEmployes [][] = new String[rownum][colcount];
        for(int i=1;i<=rownum;i++) {
            for(int j=0;j<colcount;j++) {
                dataEmployes[i-1][j] = Utility.getCellData(path,"Sheet1",i,j);
            }
        }
        return(dataEmployes);
    }

    @Test(dataProvider = "transactionHistoryStartDateMoreThanEndDate", priority = 5)
    private void getTransactionHistoryStartDateMoreThanEndDate(String category, String startDate, String endDate, String page) throws JSONException {
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        if(startDate.equals("valid"))
        {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
            LocalDateTime now = LocalDateTime.now();
            startDate = dtf.format(now);
        }

        if(endDate.equals("valid"))
        {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
            LocalDateTime now = LocalDateTime.now();
            endDate = dtf.format(now);
        }

        transactionHistory(category,startDate,endDate,page,idUser,token);

        //checking response body
        checkBody("Start date cannot be more current than end date.");

        //checking status code
        checkDevStatus("078");
        checkStatusCode("400");

        //check timestamp
        checkTimestamp();

        //check path
        checkPath(getPathTransactionHistory(idUser));

        //check response data
        checkEmptyData();

        //checking response time
        checkResponseTime("3000");
    }


    @DataProvider(name="transactionHistoryStartDateMoreThanEndDate")
    private Object[][] getDataEmptyDateStartDateMoreThanEndDate() throws IOException {
        String path = "src/test/java/com/danaDeals/test/order/transactionHistory/transactionHistoryStartDateMoreThanEndDate.xlsx";
        int rownum = Utility.getRowCount(path, "Sheet1");
        int colcount = Utility.getCellCount(path, "Sheet1",1);
        String dataEmployes [][] = new String[rownum][colcount];
        for(int i=1;i<=rownum;i++) {
            for(int j=0;j<colcount;j++) {
                dataEmployes[i-1][j] = Utility.getCellData(path,"Sheet1",i,j);
            }
        }
        return(dataEmployes);
    }
    @Test(priority = 6)
    private void transactionHistoryWrongTokenAndId(){
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt").toLowerCase();
        String category = "IN-PROGRESS";
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDateTime now = LocalDateTime.now();
        String date = dtf.format(now);
        String page = "0";

        for(int i=0;i<3;i++) {
            if(i==1){
                token = token.toUpperCase();
            }
            else if(i==2){
                idUser=Integer.toString(Integer.parseInt(idUser)-1);
                token=loadFile("token.txt");
            }

            transactionHistory(category, date,date,page,idUser,token);
            //check response body
            checkBody("You are not authorized.");

            //check status
            checkDevStatus("021");
            checkStatusCode("401");

            //check timestamp
            checkTimestamp();

            //check path
            checkPath(getPathTransactionHistory(idUser));

            //check response data
            checkEmptyData();

            //check response time
            checkResponseTime("3000");
        }
    }
}

package com.danaDeals.test.order.createOrder;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.RestUtils;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import org.json.JSONException;
import org.json.simple.JSONObject;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class TS_01_Create_Order_Success extends TestBase {

    @BeforeClass
    private void postCreateOrder() throws InterruptedException {
        logger.info("********" + getClass().getName() + "*********");
        Thread.sleep(5);
    }

    @Test(priority = 0)
    private void createOrderSuccess() throws JSONException {
        String idVoucher;
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");
        for(int i=0 ; i<3;i++)
        {
            idVoucher=Integer.toString(i+1);
            if(i==1) {idVoucher="10";}
            createOrderVoucher(idVoucher,idUser,token);

            //check response body
            checkBody("A new transaction has been created!");

            //check status
            checkDevStatus("052");
            checkStatusCode("201");

            //check response time
            checkResponseTime("3000");

            setIdTransaction();
            if(i==0) {writeFile(Integer.toString(getIdTransaction()),"idTransaction.txt");}
            else if(i==1) {writeFile(Integer.toString(getIdTransaction()),"idTransactionRefund.txt");}
            else if(i==2) {writeFile(Integer.toString(getIdTransaction()),"idTransactionFailed.txt");}
        }

    }

}

package com.danaDeals.test.order.topUpEwallet;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.RestUtils;
import org.json.JSONException;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TS_01_Top_Up_Success extends TestBase {

    @BeforeClass
    private void postTopUp() throws InterruptedException {
        logger.info("********" + getClass().getName() + "*********");
        Thread.sleep(5);
    }

    @Test(priority = 0)
    private void topUpSuccessBni() throws JSONException {
        String telephone = loadFile("telephone.txt");
        String virtualNumber = RestUtils.virtualNumberGenerator(telephone,"bni");
        String amount = "10000";
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");
        for(int i=0;i<4;i++) {
            if(i==1){
                virtualNumber = RestUtils.virtualNumberGenerator(telephone,"mandiri");
            }
            else if(i==2){
                virtualNumber = RestUtils.virtualNumberGenerator(telephone,"bca");
            }
            else{
                virtualNumber = RestUtils.virtualNumberGenerator(telephone,"alfamart");
            }
            topUp(virtualNumber, amount, idUser, token);

            //check response body
            checkBody("Your TOPUP is successful.");

            //check status
            checkDevStatus("033");
            checkStatusCode("201");

            //checking response data
            checkEmptyData();

            //checking path
            checkPath(getPathTopUp(idUser));

            //checking timestamp
            checkTimestamp();

            //updating balance
            Double balance = Double.parseDouble(loadFile("balance.txt")) + Double.parseDouble(amount);
            writeFile(Double.toString(balance), "balance.txt");

            //check response time
            checkResponseTime("3000");
        }
    }


}

package com.danaDeals.test.order.topUpEwallet;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.RestUtils;
import com.danaDeals.utilities.Utility;
import org.json.JSONException;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;

public class TS_02_Top_Up_Failed extends TestBase {

    @BeforeClass
    private void postTopUp() throws InterruptedException {
        logger.info("********" + getClass().getName() + "*********");
        Thread.sleep(5);
    }

    @Test(dataProvider = "dataTopUpMaxBalance", priority = 0)
    private void dataDrivenTopUpReachMaximumBalance(String amount) throws InterruptedException {

        String telephone = loadFile("telephone.txt");
        String virtualNumber = RestUtils.virtualNumberGenerator(telephone,"bni");

        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        topUp(virtualNumber, amount,idUser,token);

        //checking response body
        checkBody("Top-up failed! You have reached your maximum balance.");

        //checking response data
        checkEmptyData();

        //checking status code
        checkDevStatus("034");
        checkStatusCode("403");

        //checking response data
        checkEmptyData();

        //checking path
        checkPath(getPathTopUp(idUser));

        //checking timestamp
        checkTimestamp();

        //checking response time
        checkResponseTime("3000");

        Thread.sleep(5);
    }

    @DataProvider(name = "dataTopUpMaxBalance")
    private Object[][] getDataEmptyMaxBalance() throws IOException {
        String path = "src/test/java/com/danaDeals/test/order/topUpEwallet/dataTopUpMaxBalance.xlsx";
        int rownum = Utility.getRowCount(path, "Sheet1");
        int colcount = Utility.getCellCount(path, "Sheet1", 1);
        String dataEmployes[][] = new String[rownum][colcount];
        for (int i = 1; i <= rownum; i++) {
            for (int j = 0; j < colcount; j++) {
                dataEmployes[i - 1][j] = Utility.getCellData(path, "Sheet1", i, j);
            }
        }
        return (dataEmployes);
    }

    @Test(dataProvider = "dataTopUpMinBalance", priority = 1)
    private void dataDrivenTopUpReachMinimumBalance(String amount) throws InterruptedException {

        String telephone = loadFile("telephone.txt");
        String virtualNumber = RestUtils.virtualNumberGenerator(telephone,"bni");

        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        topUp(virtualNumber, amount,idUser,token);
        //checking response body
        checkBody("Minimum top-up amount is Rp 10.000,00.");

        //checking status code
        checkDevStatus("035");
        checkStatusCode("403");

        //checking response data
        checkEmptyData();

        //checking path
        checkPath(getPathTopUp(idUser));

        //checking timestamp
        checkTimestamp();

        //checking response data
        checkEmptyData();

        //checking response time
        checkResponseTime("3000");

        Thread.sleep(5);
    }


    @DataProvider(name = "dataTopUpMinBalance")
    private Object[][] getDataEmptyMinBalance() throws IOException {
        String path = "src/test/java/com/danaDeals/test/order/topUpEwallet/dataTopUpMinBalance.xlsx";
        int rownum = Utility.getRowCount(path, "Sheet1");
        int colcount = Utility.getCellCount(path, "Sheet1", 1);
        String dataEmployes[][] = new String[rownum][colcount];
        for (int i = 1; i <= rownum; i++) {
            for (int j = 0; j < colcount; j++) {
                dataEmployes[i - 1][j] = Utility.getCellData(path, "Sheet1", i, j);
            }
        }
        return (dataEmployes);
    }
    @Test(dataProvider = "dataTopUpVirtualNumberError", priority = 2)
    private void dataDrivenTopUpVirtualNumberError(String code) throws InterruptedException {
        String telephone = loadFile("telephone.txt");
        String amount = "10000";
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        String virtualNumber = code+telephone;

        topUp(virtualNumber, amount,idUser,token);

        //checking response body
        checkBody("The merchant is currently not available for balance top-up.");

        //checking status code
        checkDevStatus("036");
        checkStatusCode("406");

        //checking response data
        checkEmptyData();

        //checking path
        checkPath(getPathTopUp(idUser));

        //checking timestamp
        checkTimestamp();

        //checking response data
        checkEmptyData();

        //checking response time
        checkResponseTime("3000");

        Thread.sleep(5);
    }

    @DataProvider(name = "dataTopUpVirtualNumberError")
    private Object[][] getDataEmptyVirtualNumberError() throws IOException {
        String path = "src/test/java/com/danaDeals/test/order/topUpEwallet/dataTopUpVirtualNumberError.xlsx";
        int rownum = Utility.getRowCount(path, "Sheet1");
        int colcount = Utility.getCellCount(path, "Sheet1", 1);
        String dataEmployes[][] = new String[rownum][colcount];
        for (int i = 1; i <= rownum; i++) {
            for (int j = 0; j < colcount; j++) {
                dataEmployes[i - 1][j] = Utility.getCellData(path, "Sheet1", i, j);
            }
        }
        return (dataEmployes);
    }

    @Test(priority = 3)
    private void topUpNotAuthorized() throws JSONException {
        String telephone = loadFile("telephone.txt");
        String virtualNumber = RestUtils.virtualNumberGenerator(telephone,"mandiri");
        String amount = RestUtils.amountGenerator();
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");
        for(int i=0;i<4;i++) {
            if(i==0){
                token=token.toUpperCase();
            }
            else if(i==1){
                token=token.toLowerCase();
            }
            else if(i==2){
                token=loadFile("token.txt");
                idUser=Integer.toString(Integer.parseInt(idUser)-1);
            }
            else{
                idUser=loadFile("idUser.txt");
                token=loadFile("oldToken.txt");
            }
            topUp(virtualNumber, amount, idUser, token);

            //checking response body
            checkBody("You are not authorized.");

            //checking response data
            checkEmptyData();

            //checking response data
            checkEmptyData();

            //checking path
            checkPath(getPathTopUp(idUser));

            //checking timestamp
            checkTimestamp();

            //checking status code
            checkDevStatus("021");
            checkStatusCode("401");

            //checking response time
            checkResponseTime("3000");
        }
    }

    @Test(dataProvider = "dataTopUpInvalidBalance", priority = 4)
    private void dataDrivenTopUpInvalidBalance(String amount) throws InterruptedException {
        String telephone = loadFile("telephone.txt");
        String virtualNumber = RestUtils.virtualNumberGenerator(telephone,"bni");
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");


        topUp(virtualNumber, amount,idUser,token);

        //checking response body
        checkBody("Amount is invalid.");

        //checking response data
        checkEmptyData();

        //checking status code
        checkDevStatus("066");
        checkStatusCode("400");

        //checking response data
        checkEmptyData();

        //checking path
        checkPath(getPathTopUp(idUser));

        //checking timestamp
        checkTimestamp();

        //checking response time
        checkResponseTime("3000");

        Thread.sleep(5);
    }

    @DataProvider(name = "dataTopUpInvalidBalance")
    private Object[][] getDataEmptyInvalidBalance() throws IOException {
        String path = "src/test/java/com/danaDeals/test/order/topUpEwallet/dataTopUpInvalidBalance.xlsx";
        int rownum = Utility.getRowCount(path, "Sheet1");
        int colcount = Utility.getCellCount(path, "Sheet1", 1);
        String dataEmployes[][] = new String[rownum][colcount];
        for (int i = 1; i <= rownum; i++) {
            for (int j = 0; j < colcount; j++) {
                dataEmployes[i - 1][j] = Utility.getCellData(path, "Sheet1", i, j);
            }
        }
        return (dataEmployes);
    }
    @Test(priority = 5)
    private void dataDrivenTopUpInvalidVirtualAccount() throws InterruptedException {
        String telephone = loadFile("telephone.txt");
        String virtualNumber = "abasdadasd";
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");
        String amount = "10000";

        topUp(virtualNumber, amount,idUser,token);

        //checking response body
        checkBody("Virtual Account is Invalid.");

        //checking response data
        checkEmptyData();

        //checking status code
        checkDevStatus("067");
        checkStatusCode("400");

        //checking response data
        checkEmptyData();

        //checking path
        checkPath(getPathTopUp(idUser));

        //checking timestamp
        checkTimestamp();

        //checking response time
        checkResponseTime("3000");

        Thread.sleep(5);
    }
}

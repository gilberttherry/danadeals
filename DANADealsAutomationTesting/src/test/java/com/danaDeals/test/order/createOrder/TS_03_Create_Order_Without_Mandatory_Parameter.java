package com.danaDeals.test.order.createOrder;

import com.danaDeals.base.TestBase;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TS_03_Create_Order_Without_Mandatory_Parameter extends TestBase {

    @BeforeClass
    private void postCreateOrder() throws InterruptedException {
        logger.info("********" + getClass().getName() + "*********");
        Thread.sleep(5);
    }
    @Test(priority = 0)
    private void createOrderWithoutParameter(){
        String token = loadFile("token.txt");
        String idUser = loadFile("idUser.txt");
        for(int i=0;i<2;i++) {
            RestAssured.baseURI = BaseURI;
            httpRequest = RestAssured.given();

            httpRequest.header("Authorization", "Bearer " + token);

            httpRequest.header("Content-Type", "application/json");
            httpRequest.body(requestParams.toJSONString());
            if(i==1){
                requestParams.put("idVoucher", null);
            }
            response = httpRequest.request(Method.POST, "/api/user/" + idUser + "/transaction/voucher");
            responseBody = response.getBody().asString();

            //check response body
            checkBody("Please fill in all the forms!");

            //check status code
            checkDevStatus("002");
            checkStatusCode("400");

            //check timestamp
            checkTimestamp();

            //check path
            checkPath(getPathCreateOrder(idUser));

            //check response data
            checkEmptyData();

            //check response time
            checkResponseTime("3000");
        }
    }
}

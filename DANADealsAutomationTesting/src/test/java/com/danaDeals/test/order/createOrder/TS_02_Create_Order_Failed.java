package com.danaDeals.test.order.createOrder;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.RestUtils;
import com.danaDeals.utilities.Utility;
import org.json.JSONException;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;

public class TS_02_Create_Order_Failed extends TestBase {

    @BeforeClass
    private void postCreateOrder() throws InterruptedException {
        logger.info("********" + getClass().getName() + "*********");
        Thread.sleep(5);
    }

    @Test(dataProvider = "dataCreateOrderIdError", priority = 0)
    private void dataDrivenCreateOrderIdError(String idVoucher) throws InterruptedException {

        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        createOrderVoucher(idVoucher,idUser,token);

        //checking response body
        checkBody("The voucher is currently not available.");

        //checking status code
        checkDevStatus("027");
        checkStatusCode("404");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathCreateOrder(idUser));

        //checking response data
        checkEmptyData();

        //checking response time
        checkResponseTime("3000");

        Thread.sleep(5);
    }

    @DataProvider(name = "dataCreateOrderIdError")
    private Object[][] getDataEmptyMaxBalance() throws IOException {
        String path = "src/test/java/com/danaDeals/test/order/createOrder/dataCreateOrderIdError.xlsx";
        int rownum = Utility.getRowCount(path, "Sheet1");
        int colcount = Utility.getCellCount(path, "Sheet1", 1);
        String dataEmployes[][] = new String[rownum][colcount];
        for (int i = 1; i <= rownum; i++) {
            for (int j = 0; j < colcount; j++) {
                dataEmployes[i - 1][j] = Utility.getCellData(path, "Sheet1", i, j);
            }
        }
        return (dataEmployes);
    }

    @Test(priority = 1)
    private void createOrderOutOfStockVoucher() throws JSONException {
        String idVoucher = loadFile("outOfStockVoucher.txt");
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        createOrderVoucher(idVoucher,idUser,token);

        //check response body
        checkBody("The voucher is currently out of stock.");

        //check status
        checkDevStatus("048");
        checkStatusCode("404");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathCreateOrder(idUser));

        //checking response data
        checkEmptyData();

        //check response time
        checkResponseTime("3000");

    }

    @Test(priority = 2)
    private void createOrderInactiveVoucher() throws JSONException {
        String idVoucher = loadFile("inactiveVoucher.txt");
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");
        for(int i=0;i<2;i++) {
            if (i == 1) {
                idVoucher = loadFile("expiredVoucher.txt");
            }

            createOrderVoucher(idVoucher, idUser, token);

            //check response body
            checkBody("The voucher is currently not available.");

            //check status
            checkDevStatus("027");
            checkStatusCode("404");

            //checking timestamp
            checkTimestamp();

            //checking path
            checkPath(getPathCreateOrder(idUser));

            //checking response data
            checkEmptyData();

            //check response time
            checkResponseTime("3000");
        }

    }

    @Test(priority = 3)
    private void createOrderInvalidIdUser() throws JSONException {
        String idVoucher = loadFile("idVoucherSuccess.txt");
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        createOrderVoucher(idVoucher,Integer.toString(Integer.parseInt(idUser)+1),token);

        //check response body
        checkBody("You are not authorized.");

        //check status
        checkDevStatus("021");
        checkStatusCode("401");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathCreateOrder(Integer.toString(Integer.parseInt(idUser)+1)));

        //checking response data
        checkEmptyData();

        //check response time
        checkResponseTime("3000");

    }

    @Test(priority = 5)
    private void createOrderOldToken() throws JSONException {
        String idVoucher = loadFile("idVoucherSuccess.txt");
        String idUser = loadFile("idUser.txt");
        String token = loadFile("oldToken.txt");
        for(int i=0;i<4;i++) {
            if(i==1){
                token=loadFile("token.txt").toLowerCase();
            }
            else if(i==2){
                token=loadFile("token.txt").toUpperCase();
            }
            else
            {
                token=loadFile("token.txt");
                idUser=Integer.toString(Integer.parseInt(idUser)-1);
            }
            createOrderVoucher(idVoucher, idUser, token);

            //check response body
            checkBody("You are not authorized.");

            //check status
            checkDevStatus("021");
            checkStatusCode("401");

            //checking timestamp
            checkTimestamp();

            //checking path
            checkPath(getPathCreateOrder(idUser));

            //checking response data
            checkEmptyData();

            //check response time
            checkResponseTime("3000");
        }

    }

}

package com.danaDeals.test.order.topUpEwallet;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.RestUtils;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import org.json.JSONException;
import org.json.simple.JSONObject;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TS_03_Top_up_Without_Mandatory_Parameter extends TestBase {

    @BeforeClass
    private void postTopUp() throws InterruptedException {
        logger.info("********" + getClass().getName() + "*********");
        Thread.sleep(5);
    }

    @Test(priority = 0)
    private void topUpWithoutVirtualNumber() throws JSONException {
        String idUser = loadFile("idUser.txt");
        String amount = RestUtils.amountGenerator();
        String token = loadFile("token.txt");
        String telephone = loadFile("telephone.txt");
        String virtualNumber = RestUtils.virtualNumberGenerator(telephone, "bni");

        for (int i = 0; i < 7; i++) {
            RestAssured.baseURI = BaseURI;
            httpRequest = RestAssured.given();
            requestParams = new JSONObject();
            httpRequest.header("Authorization", "Bearer " + token);
            if (i == 0) {
                requestParams.put("virtualNumber", virtualNumber);
            } else if (i == 1) {
                requestParams.put("amount", amount);
            } else if (i == 2) {
                requestParams.put("amount", null);
                requestParams.put("virtualNumber", virtualNumber);
            } else if (i == 3) {
                requestParams.put("amount", amount);
                requestParams.put("virtualNumber", null);
            } else if (i == 4) {
                requestParams.put("amount", null);
                requestParams.put("virtualNumber", null);
            } else if (i == 5) {
                requestParams.put("amount", null);
            } else if (i == 6) {
                requestParams.put("virtualNumber", null);
            }

            httpRequest.header("Content-Type", "application/json");
            httpRequest.body(requestParams.toJSONString());

            response = httpRequest.request(Method.POST, "/api/user/" + idUser + "/transaction/topup");
            responseBody = response.getBody().asString();

            //check response body
            checkBody("Please fill in all the forms!");

            //check status code
            checkDevStatus("002");
            checkStatusCode("400");

            //checking response data
            checkEmptyData();

            //checking path
            checkPath(getPathTopUp(idUser));

            //checking timestamp
            checkTimestamp();

            //check response time
            checkResponseTime("3000");
        }
    }
}

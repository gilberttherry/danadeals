package com.danaDeals.test.order.transcationDetail;

import com.danaDeals.base.TestBase;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TS_01_Transaciton_Detail_Success extends TestBase {

    @BeforeClass
    private void getTransactionDetail() throws InterruptedException {
        logger.info("********" + getClass().getName() + "*********");
        Thread.sleep(5);
    }

    @Test(priority = 0)
    private void transactionDetailSuccess(){
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");
        String idTransaction = loadFile("idTransaction.txt");

        transactionDetail(idTransaction,idUser,token);

        //check response body
        checkBody("Transaction history are successfully collected.");

        //check status
        checkDevStatus("039");
        checkStatusCode("200");

        //check timestamp
        checkTimestamp();

        //check path
        checkPath(getPathTransactionDetail(idUser,idTransaction));

        //check response data
        checkTransactionDetail(idTransaction,"In progress");

        //check response time
        checkResponseTime("3000");

    }

}

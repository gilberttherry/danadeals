package com.danaDeals.test.endToEnd;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.RestUtils;
import org.json.JSONException;
import org.testng.annotations.Test;

public class TS_042_Register_ShowProfile extends TestBase {

    @Test(priority = 0)
    private void registerSuccess() throws JSONException {
        String email = RestUtils.gmailGenerator();
        String telephone = RestUtils.telephoneNumber();
        String name = RestUtils.nameGenerator();
        String password = RestUtils.passwordGenerator();

        register(name, email, telephone, password,password);

        setIdUser();
        writeFile(Integer.toString(getIdUser()),"idUser.txt");

        //checking response body
        checkBody("Registration is successful.");

        //checking status code
        checkDevStatus("001");
        checkStatusCode("201");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathRegis());

        //checking response data
        checkRegisData(name,email,telephone,Integer.toString(getIdUser()));

        //checking response time
        checkResponseTime("3000");

        setIdUser();
        writeFile(Integer.toString(getIdUser()),"idUser.txt");
    }

    @Test(priority = 1)
    private void showProfile(){
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        showProfile(idUser,token);

        //checking response body
        checkBody("You are not authorized.");

        //checking status code
        checkDevStatus("021");
        checkStatusCode("401");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathshowProfile(idUser));

        //checking response data
        checkEmptyData();

        //checking response time
        checkResponseTime("3000");
    }
}

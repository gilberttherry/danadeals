package com.danaDeals.test.endToEnd;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.RestUtils;
import org.json.JSONException;
import org.testng.annotations.Test;

public class TS_016_Register_RequestOtp_ForgotPassword extends TestBase {

    @Test(priority = 0)
    private void registerSuccess() throws JSONException {
        String email = RestUtils.gmailGenerator();
        String telephone = RestUtils.telephoneNumber();
        String name = RestUtils.nameGenerator();
        String password = RestUtils.passwordGenerator();

        register(name, email, telephone, password,password);

        setIdUser();
        writeFile(Integer.toString(getIdUser()),"idUser.txt");

        //checking response body
        checkBody("Registration is successful.");

        //checking status code
        checkDevStatus("001");
        checkStatusCode("201");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathRegis());

        //checking response data
        checkRegisData(name,email,telephone,Integer.toString(getIdUser()));

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 1)
    private void requestOtp() throws JSONException {
        String telephone = loadFile("telephone.txt");

        requestOtp(telephone);

        setIdUserReqOtp();
        writeFile(Integer.toString(getIdUser()),"idUser.txt");

        //checking response body
        checkBody("Your OTP has sent to your phone number.");

        //checking status code
        checkDevStatus("012");
        checkStatusCode("200");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathRequestOtp());

        //checking empty data
        checkRequestOtp(Integer.toString(getIdUser()));

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 2)
    private void forgotPassword(){
        String newPassword = RestUtils.passwordGenerator();
        String idUser = loadFile("idUser.txt");

        forgotPassword(newPassword,newPassword,idUser);

        //checking response body
        checkBody("Please fill otp.");

        //checking status code
        checkDevStatus("051");
        checkStatusCode("400");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathForgotPassword(idUser));

        //checking empty data
        checkEmptyData();

        //checking response time
        checkResponseTime("3000");
    }


}

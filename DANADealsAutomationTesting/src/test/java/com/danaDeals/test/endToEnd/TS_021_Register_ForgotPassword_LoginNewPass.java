package com.danaDeals.test.endToEnd;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.RestUtils;
import org.json.JSONException;
import org.testng.annotations.Test;

public class TS_021_Register_ForgotPassword_LoginNewPass extends TestBase {

    @Test(priority = 0)
    private void registerSuccess() throws JSONException {
        String email = RestUtils.gmailGenerator();
        String telephone = RestUtils.telephoneNumber();
        String name = RestUtils.nameGenerator();
        String password = RestUtils.passwordGenerator();

        register(name, email, telephone, password,password);

        setIdUser();
        writeFile(Integer.toString(getIdUser()),"idUser.txt");

        //checking response body
        checkBody("Registration is successful.");

        //checking status code
        checkDevStatus("001");
        checkStatusCode("201");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathRegis());

        //checking response data
        checkRegisData(name,email,telephone,Integer.toString(getIdUser()));

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 1)
    private void forgotPassword(){
        String newPassword = RestUtils.passwordGenerator();
        String idUser = loadFile("idUser.txt");

        forgotPassword(newPassword,newPassword,idUser);
        writeFile(newPassword,"password.txt");

        //checking response body
        checkBody("Please request a new OTP!");

        //checking status code
        checkDevStatus("018");
        checkStatusCode("400");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathForgotPassword(idUser));

        //checking empty data
        checkEmptyData();

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 2)
    private void login(){
        String telephone = loadFile("telephone.txt");
        String password = loadFile("password.txt");

        login(telephone,password);

        //checking response body
        checkBody("Your data do not match our records.");

        //checking status code
        checkDevStatus("011");
        checkStatusCode("404");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathLogin());

        //checking empty data
        checkEmptyData();

        //checking response time
        checkResponseTime("3000");
    }
}

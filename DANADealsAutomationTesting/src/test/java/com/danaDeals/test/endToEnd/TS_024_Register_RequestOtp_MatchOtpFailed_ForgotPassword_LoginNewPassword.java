package com.danaDeals.test.endToEnd;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.RestUtils;
import org.json.JSONException;
import org.testng.annotations.Test;

public class TS_024_Register_RequestOtp_MatchOtpFailed_ForgotPassword_LoginNewPassword extends TestBase {

    @Test(priority = 0)
    private void registerSuccess() throws JSONException {
        String email = RestUtils.gmailGenerator();
        String telephone = RestUtils.telephoneNumber();
        String name = RestUtils.nameGenerator();
        String password = RestUtils.passwordGenerator();

        register(name, email, telephone, password,password);
        setIdUser();
        writeFile(Integer.toString(getIdUser()),"idUser.txt");

        //checking response body
        checkBody("Registration is successful.");

        //checking status code
        checkDevStatus("001");
        checkStatusCode("201");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathRegis());

        //checking response data
        checkRegisData(name,email,telephone,Integer.toString(getIdUser()));

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 1)
    private void requestOtp() throws JSONException {
        String telephone = loadFile("telephone.txt");

        requestOtp(telephone);

        setIdUserReqOtp();
        writeFile(Integer.toString(getIdUser()),"idUser.txt");
        //checking response body
        checkBody("Your OTP has sent to your phone number.");

        //checking status code
        checkDevStatus("012");
        checkStatusCode("200");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathRequestOtp());

        //checking response data
        checkRequestOtp(Integer.toString(getIdUser()));

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 2)
    private void matchOtp(){
        String idUser = loadFile("idUser.txt");

        matchOtp("0001",idUser);

        //checking response body
        checkBody("OTP not match.");

        //checking status code
        checkDevStatus("016");
        checkStatusCode("400");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathMatchOtp(idUser));

        //checking response data
        checkEmptyData();

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 3)
    private void forgotPassword(){
        String newPassword = RestUtils.passwordGenerator();
        String idUser = loadFile("idUser.txt");

        forgotPassword(newPassword,newPassword,idUser);

        writeFile(newPassword,"password.txt");

        //checking response body
        checkBody("Please fill otp.");

        //checking status code
        checkDevStatus("051");
        checkStatusCode("400");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathForgotPassword(idUser));

        //checking response data
        checkEmptyData();

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 4)
    private void login(){
        String telephone = loadFile("telephone.txt");
        String password = loadFile("password.txt");

        login(telephone,password);

        //checking response body
        checkBody("Your data do not match our records.");

        //checking status code
        checkDevStatus("011");
        checkStatusCode("404");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathLogin());

        //checking response data
        checkEmptyData();

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 5)
    private void registerSuccessExpiredOtp() throws JSONException {
        String email = RestUtils.gmailGenerator();
        String telephone = RestUtils.telephoneNumber();
        String name = RestUtils.nameGenerator();
        String password = RestUtils.passwordGenerator();

        register(name, email, telephone, password,password);

        setIdUser();
        writeFile(Integer.toString(getIdUser()),"idUser.txt");

        //checking response body
        checkBody("Registration is successful.");

        //checking status code
        checkDevStatus("001");
        checkStatusCode("201");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathRegis());

        //checking response data
        checkRegisData(name,email,telephone,Integer.toString(getIdUser()));

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 6)
    private void requestOtpExpiredOtp() throws JSONException {
        String telephone = loadFile("telephone.txt");

        requestOtp(telephone);

        //checking response body
        checkBody("Your OTP has sent to your phone number.");

        //checking status code
        checkDevStatus("012");
        checkStatusCode("200");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathRequestOtp());

        //checking response data
        checkRequestOtp(Integer.toString(getIdUser()));

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 7)
    private void matchOtpExpiredOtp() throws InterruptedException {
        String idUser = loadFile("idUser.txt");
        Thread.sleep(60000);
        matchOtp("0000",idUser);

        //checking response body
        checkBody("Your OTP expired.");

        //checking status code
        checkDevStatus("017");
        checkStatusCode("400");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathMatchOtp(idUser));

        //checking response data
        checkEmptyData();

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 8)
    private void forgotPasswordExpiredOtp(){
        String newPassword = RestUtils.passwordGenerator();
        String idUser = loadFile("idUser.txt");

        forgotPassword(newPassword,newPassword,idUser);
        writeFile(newPassword,"password.txt");

        //checking response body
        checkBody("Please request a new OTP!");

        //checking status code
        checkDevStatus("018");
        checkStatusCode("400");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathForgotPassword(idUser));

        //checking response data
       checkEmptyData();

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 9)
    private void loginExpiredOtp(){
        String telephone = loadFile("telephone.txt");
        String password = loadFile("password.txt");

        login(telephone,password);

        //checking response body
        checkBody("Your data do not match our records.");

        //checking status code
        checkDevStatus("011");
        checkStatusCode("404");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathLogin());

        //checking response data
        checkEmptyData();

        //checking response time
        checkResponseTime("3000");
    }
}

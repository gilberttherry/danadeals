package com.danaDeals.test.endToEnd;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.RestUtils;
import org.json.JSONException;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TS_140_Register_LoginWaitTokenExpired_RequestOtp_MatchOtp_ForgotPassword_LoginNewPass extends TestBase {
    @Test(priority = 0)
    private void registerSuccess() throws JSONException {
        String email = RestUtils.gmailGenerator();
        String telephone = RestUtils.telephoneNumber();
        String name = RestUtils.nameGenerator();
        String password = RestUtils.passwordGenerator();

        register(name, email, telephone, password,password);

        setIdUser();
        writeFile(Integer.toString(getIdUser()),"idUser.txt");

        //checking response body
        checkBody("Registration is successful.");

        //checking status code
        checkDevStatus("001");
        checkStatusCode("201");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathRegis());

        //checking response data
        checkRegisData(name,email,telephone,Integer.toString(getIdUser()));

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 1)
    private void login() throws JSONException {
        String telephone = loadFile("telephone.txt");
        String password = loadFile("password.txt");

        login(telephone,password);

        setIdUser();
        writeFile(Integer.toString(getIdUser()),"idUser.txt");
        setToken();
        writeFile(getToken(),"token.txt");

        //checking response body
        checkBody("You are logged in.");

        //checking status code
        checkDevStatus("010");
        checkStatusCode("200");

        //checking timestamp
        checkTimestamp();

        //checking login data
        checkLoginData(telephone,Integer.toString(getIdUser()));

        //checking path
        checkPath(getPathLogin());

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 2)
    private void requestOtp() throws JSONException, InterruptedException {
        Thread.sleep(180000);
        String telephone = loadFile("telephone.txt");

        requestOtp(telephone);
        setIdUserReqOtp();
        Assert.assertEquals(Integer.toString(getIdUser()),loadFile("idUser.txt"));

        //checking response body
        checkBody("Your OTP has sent to your phone number.");

        //checking status code
        checkDevStatus("012");
        checkStatusCode("200");

        //checking timestamp
        checkTimestamp();

        //checking empty data
        checkRequestOtp(Integer.toString(getIdUser()));

        //checking path
        checkPath(getPathRequestOtp());

        //checking response time
        checkResponseTime("3000");
    }
    @Test(priority = 3)
    private void matchOtp(){
        String idUser = loadFile("idUser.txt");

        matchOtp("0000",idUser);

        //checking response body
        checkBody("OTP Match.");

        //checking status code
        checkDevStatus("015");
        checkStatusCode("200");

        //checking timestamp
        checkTimestamp();

        //checking empty data
        checkEmptyData();

        //checking path
        checkPath(getPathMatchOtp(idUser));

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 4)
    private void forgotPassword(){
        String newPassword = RestUtils.passwordGenerator();
        String idUser = loadFile("idUser.txt");

        forgotPassword(newPassword,newPassword,idUser);
        writeFile(newPassword,"password.txt");

        //checking response body
        checkBody("Change Password Success.");

        //checking status code
        checkDevStatus("019");
        checkStatusCode("200");

        //checking empty data
        checkEmptyData();

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathForgotPassword(idUser));

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 5)
    private void loginNewPassword() throws JSONException {
        String telephone = loadFile("telephone.txt");
        String password = loadFile("password.txt");

        login(telephone,password);

        setIdUser();
        writeFile(Integer.toString(getIdUser()),"idUser.txt");
        setToken();
        writeFile(getToken(),"token.txt");

        //checking response body
        checkBody("You are logged in.");

        //checking status code
        checkDevStatus("010");
        checkStatusCode("200");

        //checking timestamp
        checkTimestamp();

        //checking login data
        checkLoginData(telephone,Integer.toString(getIdUser()));

        //checking path
        checkPath(getPathLogin());

        //checking response time
        checkResponseTime("3000");
    }
}

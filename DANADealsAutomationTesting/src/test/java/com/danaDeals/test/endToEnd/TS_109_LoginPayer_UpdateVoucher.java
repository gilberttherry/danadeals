package com.danaDeals.test.endToEnd;

import com.danaDeals.base.TestBase;
import org.json.JSONException;
import org.testng.annotations.Test;

public class TS_109_LoginPayer_UpdateVoucher extends TestBase {

    @Test(priority = 0)
    private void loginPayer() throws JSONException {

        String telephone = loadFile("telephone.txt");
        String password = loadFile("password.txt");

        login(telephone, password);

        setIdUser();
        writeFile(Integer.toString(getIdUser()), "idUser.txt");
        setToken();
        writeFile(getToken(), "token.txt");

        //check response body
        checkBody("You are logged in.");
        checkPath("/api/auth/login");
        checkTimestamp();

        //check status
        checkDevStatus("010");
        checkStatusCode("200");

        //check response time
        checkResponseTime("3000");
    }

    @Test(priority = 1)
    private void tryUpdateVoucher() {

        String status = "true";
        String quota = loadFile("quota.txt");
        String idVoucher = loadFile("idVoucherTest.txt");
        String token = loadFile("token.txt");

        updateVoucher(1, status, quota, idVoucher, token);

        //check response body
        checkBody("You are not authorized.");
        checkPath("/api/admin/update-status-voucher/"+idVoucher+"/restock");
        checkTimestamp();

        //check status
        checkDevStatus("021");
        checkStatusCode("401");

        //check response time
        checkResponseTime("3000");
    }

    @Test(priority = 2)
    private void logoutSuccess() {

        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        logout(idUser, token);

        //checking response body
        checkBody("You are logged out.");
        checkPath("/api/user/"+idUser+"/logout");
        checkTimestamp();

        //checking status code
        checkDevStatus("041");
        checkStatusCode("200");

        //checking response time
        checkResponseTime("3000");
    }
}

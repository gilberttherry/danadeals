package com.danaDeals.test.endToEnd;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.RestUtils;
import org.json.JSONException;
import org.testng.annotations.Test;

public class TS_075_Register_Login_TopUpWallet_CreateOrder_TransactionDetail_Logout extends TestBase {

    @Test(priority = 0)
    private void registerSuccess() throws JSONException {
        String email = RestUtils.gmailGenerator();
        String telephone = RestUtils.telephoneNumber();
        String name = RestUtils.nameGenerator();
        String password = RestUtils.passwordGenerator();

        register(name, email, telephone, password,password);

        setIdUser();
        writeFile(Integer.toString(getIdUser()),"idUser.txt");

        //checking response body
        checkBody("Registration is successful.");

        //checking status code
        checkDevStatus("001");
        checkStatusCode("201");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathRegis());

        //checking response data
        checkRegisData(name,email,telephone,Integer.toString(getIdUser()));

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 1)
    private void login() throws JSONException {
        String telephone = loadFile("telephone.txt");
        String password = loadFile("password.txt");

        login(telephone,password);

        setIdUser();
        writeFile(Integer.toString(getIdUser()),"idUser.txt");
        setToken();
        writeFile(getToken(),"token.txt");

        //checking response body
        checkBody("You are logged in.");

        //checking status code
        checkDevStatus("010");
        checkStatusCode("200");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathLogin());

        //checking response data
        checkLoginData(telephone,Integer.toString(getIdUser()));

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 2)
    private void topUpWallet(){
        String telephone = loadFile("telephone.txt");
        String virtualNumber = RestUtils.virtualNumberGenerator(telephone,"bni");
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");
        String amount = "1000000";

        topUp(virtualNumber, amount,idUser,token);

        //checking response body
        checkBody("Your TOPUP is successful.");

        //checking status code
        checkDevStatus("033");
        checkStatusCode("201");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathTopUp(idUser));

        //checking response data
        checkEmptyData();

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 3)
    private void createOrderRefund() throws JSONException {
        String idVoucher = "1";
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        createOrderVoucher(idVoucher,idUser,token);

        //check response body
        checkBody("A new transaction has been created!");

        //check status
        checkDevStatus("052");
        checkStatusCode("201");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathCreateOrder(idUser));

        //check response time
        checkResponseTime("3000");

        //checking response data
        setIdTransaction();
        writeFile(Integer.toString(getIdTransaction()),"idTransaction.txt");
    }

    @Test(priority = 4)
    private void transactionDetailRefund() throws JSONException {
        String idTransaction = loadFile("idTransaction.txt");
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        transactionDetail(idTransaction,idUser,token);

        //check response body
        checkBody("Transaction history are successfully collected.");

        //check status
        checkDevStatus("039");
        checkStatusCode("200");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathTransactionDetail(idUser,idTransaction));

        //checking response data
        checkTransactionDetail(idTransaction,"In progress");

        //check response time
        checkResponseTime("3000");
    }

    @Test(priority = 5)
    private void createOrderFailedVoucher() throws JSONException {
        String idVoucher = "2";
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        createOrderVoucher(idVoucher,idUser,token);

        //check response body
        checkBody("A new transaction has been created!");

        //check status
        checkDevStatus("052");
        checkStatusCode("201");

        //check response time
        checkResponseTime("3000");

        setIdTransaction();
        writeFile(Integer.toString(getIdTransaction()),"idTransaction.txt");
    }

    @Test(priority = 6)
    private void transactionDetailFailedVoucher() throws JSONException {
        String idTransaction = loadFile("idTransaction.txt");
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        transactionDetail(idTransaction,idUser,token);

        //check response body
        checkBody("Transaction history are successfully collected.");

        //check status
        checkDevStatus("039");
        checkStatusCode("200");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathTransactionDetail(idUser,idTransaction));

        //checking response data
        checkTransactionDetail(idTransaction,"In progress");

        //check response time
        checkResponseTime("3000");
    }

    @Test(priority = 7)
    private void createOrder() throws JSONException {
        String idVoucher = loadFile("unlimitedVoucher.txt");
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        createOrderVoucher(idVoucher,idUser,token);

        //check response body
        checkBody("A new transaction has been created!");

        //check status
        checkDevStatus("052");
        checkStatusCode("201");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathCreateOrder(idUser));

        //check response time
        checkResponseTime("3000");

        //check response data
        setIdTransaction();
        writeFile(Integer.toString(getIdTransaction()),"idTransaction.txt");
    }

    @Test(priority = 8)
    private void transactionDetail() throws JSONException {
        String idTransaction = loadFile("idTransaction.txt");
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        transactionDetail(idTransaction,idUser,token);

        //check response body
        checkBody("Transaction history are successfully collected.");

        //check status
        checkDevStatus("039");
        checkStatusCode("200");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathTransactionDetail(idUser,idTransaction));

        //checking response data
        checkTransactionDetail(idTransaction,"In progress");

        //check response time
        checkResponseTime("3000");
    }
    @Test(priority = 9)
    private void logout(){
        String token = loadFile("token.txt");

        String idUser = loadFile("idUser.txt");
        logout(idUser,token);
        //checking response body
        checkBody("You are logged out.");

        //checking status code
        checkDevStatus("041");
        checkStatusCode("200");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathLogout(idUser));

        //checking response data
        checkEmptyData();

        //checking response time
        checkResponseTime("3000");
    }
}

package com.danaDeals.test.endToEnd;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.RestUtils;
import org.json.JSONException;
import org.junit.Assert;
import org.testng.annotations.Test;

public class
TS_091_Register_Login_TopUpWallet_CreateOrder_WaitUntilExpired_TransactionDetail_Logout extends TestBase {

    @Test(priority = 0)
    private void registerSuccess() throws JSONException {
        String email = RestUtils.gmailGenerator();
        String telephone = RestUtils.telephoneNumber();
        String name = RestUtils.nameGenerator();
        String password = RestUtils.passwordGenerator();

        register(name, email, telephone, password,password);

        setIdUser();
        writeFile(Integer.toString(getIdUser()),"idUser.txt");

        //checking response body
        checkBody("Registration is successful.");

        //checking status code
        checkDevStatus("001");
        checkStatusCode("201");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathRegis());

        //checking response data
        checkRegisData(name,email,telephone,Integer.toString(getIdUser()));

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 1)
    private void login() throws JSONException {
        String telephone = loadFile("telephone.txt");
        String password = loadFile("password.txt");

        login(telephone,password);

        setIdUser();
        writeFile(Integer.toString(getIdUser()),"idUser.txt");
        setToken();
        writeFile(getToken(),"token.txt");

        //checking response body
        checkBody("You are logged in.");

        //checking status code
        checkDevStatus("010");
        checkStatusCode("200");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathLogin());

        //checking response data
        checkLoginData(telephone,Integer.toString(getIdUser()));

        //checking response time
        checkResponseTime("3000");

        setToken();
        writeFile(getToken(),"oldToken.txt");
    }

    @Test(priority = 2)
    private void login2() throws JSONException {
        String telephone = loadFile("telephone.txt");
        String password = loadFile("password.txt");

        login(telephone,password);

        //checking response body
        checkBody("You login in another device.");

        //checking status code
        checkDevStatus("047");
        checkStatusCode("200");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathLogin());

        //checking response data
        checkLoginData(telephone,Integer.toString(getIdUser()));

        //checking response time
        checkResponseTime("3000");

        setToken();
        writeFile(getToken(),"token.txt");
        Assert.assertTrue(!getToken().equals(loadFile("oldToken.txt")));
    }

    @Test(priority = 3)
    private void topUp(){
        String amount = "1000000.0";
        String telephone = loadFile("telephone.txt");
        String virtualNumber = RestUtils.virtualNumberGenerator(telephone,"bni");
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        topUp(virtualNumber,amount,idUser,token);

        //check response body
        checkBody("Your TOPUP is successful.");

        //check status
        checkDevStatus("033");
        checkStatusCode("201");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathTopUp(idUser));

        //checking response data
        checkEmptyData();

        //check response time
        checkResponseTime("3000");

        writeFile(amount,"balance.txt");

    }
    @Test(priority = 4)
    private void createOrder() throws JSONException {
        String idVoucher = "3";
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        createOrderVoucher(idVoucher,idUser,token);

        //check response body
        checkBody("A new transaction has been created!");

        //check status
        checkDevStatus("052");
        checkStatusCode("201");

        //check response time
        checkResponseTime("3000");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathCreateOrder(idUser));

        //checking response data
        setIdTransaction();
        writeFile(Integer.toString(getIdTransaction()),"idTransaction.txt");
    }
    @Test(priority = 5)
    private void transactionDetailExpired() throws InterruptedException {
        Thread.sleep(30000);
        String idTransaction = loadFile("idTransaction.txt");
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        transactionDetail(idTransaction,idUser,token);

        //check response body
        checkBody("Transaction history are successfully collected.");

        //check status
        checkDevStatus("039");
        checkStatusCode("200");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathTransactionDetail(idUser,idTransaction));

        //checking response data
        checkTransactionDetail(idTransaction,"Failed");

        //check response time
        checkResponseTime("3000");
    }
    @Test(priority = 9)
    private void logout(){
        String token = loadFile("token.txt");
        String idUser = loadFile("idUser.txt");
        logout(idUser,token);

        //checking response body
        checkBody("You are logged out.");

        //checking status code
        checkDevStatus("041");
        checkStatusCode("200");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathLogout(idUser));

        //checking empty data
        checkEmptyData();

        //checking response time
        checkResponseTime("3000");
    }
    @Test(priority = 10)
    private void loginToCheckBalance() throws JSONException{
        String telephone = loadFile("telephone.txt");
        String password = loadFile("password.txt");

        login(telephone,password);

        setIdUser();
        writeFile(Integer.toString(getIdUser()),"idUser.txt");
        setToken();
        writeFile(getToken(),"token.txt");

        //checking response body
        checkBody("You are logged in.");

        //checking status code
        checkDevStatus("010");
        checkStatusCode("200");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathLogin());

        //checking response data
        checkLoginData(telephone,Integer.toString(getIdUser()));

        //checking response time
        checkResponseTime("3000");
    }
}

package com.danaDeals.test.endToEnd;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.RestUtils;
import org.json.JSONException;
import org.testng.annotations.Test;

public class TS_056_Register_Login_TopUpWallet_CreateOrder1_CreateOrder2_PayOrderOtherId extends TestBase {
    private String temp = "";
    @Test(priority = 0)
    private void registerSuccess() throws JSONException {
        String email = RestUtils.gmailGenerator();
        String telephone = RestUtils.telephoneNumber();
        String name = RestUtils.nameGenerator();
        String password = RestUtils.passwordGenerator();

        register(name, email, telephone, password,password);

        setIdUser();
        writeFile(Integer.toString(getIdUser()),"idUser.txt");

        //checking response body
        checkBody("Registration is successful.");

        //checking status code
        checkDevStatus("001");
        checkStatusCode("201");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathRegis());

        //checking response data
        checkRegisData(name,email,telephone,Integer.toString(getIdUser()));

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 1)
    private void login() throws JSONException {
        String telephone = loadFile("telephone.txt");
        String password = loadFile("password.txt");

        login(telephone,password);

        setIdUser();
        writeFile(Integer.toString(getIdUser()),"idUser.txt");
        setToken();
        writeFile(getToken(),"token.txt");

        //checking response body
        checkBody("You are logged in.");

        //checking status code
        checkDevStatus("010");
        checkStatusCode("200");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathLogin());

        //checking response data
        checkLoginData(telephone,Integer.toString(getIdUser()));

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 2)
    private void topUpWallet(){
        String telephone = loadFile("telephone.txt");
        String virtualNumber = RestUtils.virtualNumberGenerator(telephone,"bni");
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");
        String amount = "1000000";

        topUp(virtualNumber, amount,idUser,token);

        //checking response body
        checkBody("Your TOPUP is successful.");

        //checking status code
        checkDevStatus("033");
        checkStatusCode("201");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathTopUp(idUser));

        //checking response data
        checkEmptyData();

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 3)
    private void createOrder1() throws JSONException {
        String idVoucher = loadFile("unlimitedVoucher.txt");
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        createOrderVoucher(idVoucher,idUser,token);

        //check response body
        checkBody("A new transaction has been created!");

        //check status
        checkDevStatus("052");
        checkStatusCode("201");

        //check response time
        checkResponseTime("3000");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathCreateOrder(idUser));

        //checking response data
        setIdTransaction();
        writeFile(Integer.toString(getIdTransaction()),"idTransaction.txt");
        temp = Integer.toString(getIdTransaction());
    }

    @Test(priority = 4)
    private void createOrder2() throws JSONException {
        String idVoucher = loadFile("unlimitedVoucher.txt");
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        createOrderVoucher(idVoucher,idUser,token);

        //check response body
        checkBody("A new transaction has been created!");

        //check status
        checkDevStatus("052");
        checkStatusCode("201");

        //check response time
        checkResponseTime("3000");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathCreateOrder(idUser));

        //checking response data
        setIdTransaction();
        writeFile(Integer.toString(getIdTransaction()),"idTransaction.txt");
    }

    @Test(priority = 5)
    private void payOrder() throws JSONException {
        String idTransaction = Integer.toString(Integer.parseInt(this.temp)-1);
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        payOrderVoucher(idTransaction,idUser,token);

        //check response body
        checkBody("There is nothing to do with an already finished transaction.");

        //check status
        checkDevStatus("073");
        checkStatusCode("406");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathPayOrder(idUser));

        //checking response data
        checkEmptyData();

        //check response time
        checkResponseTime("3000");
    }

}

package com.danaDeals.test.endToEnd;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.RestUtils;
import org.json.JSONException;
import org.testng.annotations.Test;

public class TS_041_Register_Login_ShowProfile_Logout extends TestBase {

    @Test(priority = 0)
    private void registerSuccess() throws JSONException {
        String email = RestUtils.gmailGenerator();
        String telephone = RestUtils.telephoneNumber();
        String name = RestUtils.nameGenerator();
        String password = RestUtils.passwordGenerator();

        register(name, email, telephone, password,password);

        setIdUser();
        writeFile(Integer.toString(getIdUser()),"idUser.txt");

        //checking response body
        checkBody("Registration is successful.");

        //checking status code
        checkDevStatus("001");
        checkStatusCode("201");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathRegis());

        //checking response data
        checkRegisData(name,email,telephone,Integer.toString(getIdUser()));

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 1)
    private void login() throws JSONException {
        String telephone = loadFile("telephone.txt");
        String password = loadFile("password.txt");

        login(telephone,password);

        setIdUser();
        writeFile(Integer.toString(getIdUser()),"idUser.txt");
        setToken();
        writeFile(getToken(),"token.txt");

        //checking response body
        checkBody("You are logged in.");

        //checking status code
        checkDevStatus("010");
        checkStatusCode("200");
        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathLogin());

        //checking response data
        checkLoginData(telephone,Integer.toString(getIdUser()));

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 2)
    private void showProfile(){
        String token = loadFile("token.txt");
        String idUser = loadFile("idUser.txt");

        showProfile(idUser,token);
        //checking response body
        checkBody("Successfully display profile data.");

        //checking status code
        checkDevStatus("020");
        checkStatusCode("200");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathshowProfile(idUser));

        //checking response data
        checkShowProfileData();

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 3)
    private void logout2(){
        String token = loadFile("token.txt");

        String idUser = loadFile("idUser.txt");
        logout(idUser,token);
        //checking response body
        checkBody("You are logged out.");

        //checking status code
        checkDevStatus("041");
        checkStatusCode("200");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathLogout(idUser));

        //checking response data
        checkEmptyData();

        //checking response time
        checkResponseTime("3000");
    }
}

package com.danaDeals.test.endToEnd;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.RestUtils;
import org.json.JSONException;
import org.testng.annotations.Test;

public class TS_136_RegisterUser1_LoginUser1_TopUpWalletUser1_CreateOrderUser1_PayOrderUser1_RegisterUser2_LoginUser2_TopUpWalletUser2_PayOrderUser2 extends TestBase {

    @Test(priority = 0)
    private void registerSuccessUser1() throws JSONException {
        String email = RestUtils.gmailGenerator();
        String telephone = RestUtils.telephoneNumber();
        String name = RestUtils.nameGenerator();
        String password = RestUtils.passwordGenerator();

        register(name, email, telephone, password,password);

        setIdUser();
        writeFile(Integer.toString(getIdUser()),"idUser.txt");

        //checking response body
        checkBody("Registration is successful.");

        //checking status code
        checkDevStatus("001");
        checkStatusCode("201");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathRegis());

        //checking response data
        checkRegisData(name,email,telephone,Integer.toString(getIdUser()));
        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 1)
    private void loginUser1() throws JSONException {
        String telephone = loadFile("telephone.txt");
        String password = loadFile("password.txt");

        login(telephone,password);

        setIdUser();
        writeFile(Integer.toString(getIdUser()),"idUser.txt");
        setToken();
        writeFile(getToken(),"token.txt");

        //checking response body
        checkBody("You are logged in.");

        //checking status code
        checkDevStatus("010");
        checkStatusCode("200");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathLogin());

        //checking response data
        checkLoginData(telephone,Integer.toString(getIdUser()));

        //checking response time
        checkResponseTime("3000");

        setToken();
        writeFile(getToken(),"oldToken.txt");
    }
    @Test(priority = 2)
    private void topUpUser1(){
        String amount = "10000.000000";
        String telephone = loadFile("telephone.txt");
        String virtualNumber = RestUtils.virtualNumberGenerator(telephone,"bni");
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        topUp(virtualNumber,amount,idUser,token);

        //check response body
        checkBody("Your TOPUP is successful.");

        //check status
        checkDevStatus("033");
        checkStatusCode("201");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathTopUp(idUser));

        //checking response data
        checkEmptyData();

        //check response time
        checkResponseTime("3000");

    }
    @Test(priority = 3)
    private void createOrderUser1() throws JSONException {
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");
        String idVoucher = loadFile("unlimitedVoucher.txt");
        createOrderVoucher(idVoucher,idUser,token);

        //checking response body
        checkBody("A new transaction has been created!");

        //checking status code
        checkDevStatus("052");
        checkStatusCode("201");

        //checking response time
        checkResponseTime("3000");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathCreateOrder(idUser));

        //checking response data
        setIdTransaction();
        writeFile(Integer.toString(getIdTransaction()),"idTransaction.txt");
    }

    @Test(priority = 4)
    private void payOrderUser1(){
        String idTransaction = loadFile("idTransaction.txt");
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        payOrderVoucher(idTransaction,idUser,token);

        //check response body
        checkBody("Your payment is successfull.");

        //check status
        checkDevStatus("028");
        checkStatusCode("200")
        ;
        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathPayOrder(idUser));

        //checking response data
        checkEmptyData();

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 5)
    private void registerSuccessUser2() throws JSONException {
        String email = RestUtils.gmailGenerator();
        String telephone = RestUtils.telephoneNumber();
        String name = RestUtils.nameGenerator();
        String password = RestUtils.passwordGenerator();

        register(name, email, telephone, password,password);

        setIdUser();
        writeFile(Integer.toString(getIdUser()),"idUser.txt");

        //checking response body
        checkBody("Registration is successful.");

        //checking status code
        checkDevStatus("001");
        checkStatusCode("201");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathRegis());

        //checking response data
        checkRegisData(name,email,telephone,Integer.toString(getIdUser()));
        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 6)
    private void loginUser2() throws JSONException {
        String telephone = loadFile("telephone.txt");
        String password = loadFile("password.txt");

        login(telephone,password);

        setIdUser();
        writeFile(Integer.toString(getIdUser()),"idUser.txt");
        setToken();
        writeFile(getToken(),"token.txt");

        //checking response body
        checkBody("You are logged in.");

        //checking status code
        checkDevStatus("010");
        checkStatusCode("200");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathLogin());

        //checking response data
        checkLoginData(telephone,Integer.toString(getIdUser()));

        //checking response time
        checkResponseTime("3000");

        setToken();
        writeFile(getToken(),"oldToken.txt");
    }
    @Test(priority = 7)
    private void topUpUser2(){
        String amount = "10000.000000";
        String telephone = loadFile("telephone.txt");
        String virtualNumber = RestUtils.virtualNumberGenerator(telephone,"bni");
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        topUp(virtualNumber,amount,idUser,token);

        //check response body
        checkBody("Your TOPUP is successful.");

        //check status
        checkDevStatus("033");
        checkStatusCode("201");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathTopUp(idUser));

        //checking response data
        checkEmptyData();

        //check response time
        checkResponseTime("3000");

    }

    @Test(priority = 8)
    private void payOrderUser2(){
        String idTransaction = loadFile("idTransaction.txt");
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        payOrderVoucher(idTransaction,idUser,token);

        //check response body
        checkBody("Transaction is not found.");

        //check status
        checkDevStatus("032");
        checkStatusCode("404")
        ;
        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathPayOrder(idUser));

        //checking response data
        checkEmptyData();

        //checking response time
        checkResponseTime("3000");
    }
}

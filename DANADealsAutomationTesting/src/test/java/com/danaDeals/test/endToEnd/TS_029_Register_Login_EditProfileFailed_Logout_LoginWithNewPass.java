package com.danaDeals.test.endToEnd;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.RestUtils;
import org.json.JSONException;
import org.testng.annotations.Test;

public class TS_029_Register_Login_EditProfileFailed_Logout_LoginWithNewPass extends TestBase {
    private String email = "";
    @Test(priority = 0)
    private void registerSuccess1() throws JSONException {
        String email = RestUtils.gmailGenerator();
        String telephone = RestUtils.telephoneNumber();
        String name = RestUtils.nameGenerator();
        String password = RestUtils.passwordGenerator();
        this.email=email;
        register(name, email, telephone, password,password);
        for(int i=0;i<2;i++) {

            setIdUser();
            writeFile(Integer.toString(getIdUser()), "idUser.txt");

            //checking response body
            checkBody("Registration is successful.");

            //checking status code
            checkDevStatus("001");
            checkStatusCode("201");

            //checking timestamp
            checkTimestamp();

            //checking path
            checkPath(getPathRegis());

            //checking response data
            checkRegisData(name, email, telephone, Integer.toString(getIdUser()));

            //checking response time
            checkResponseTime("3000");
        }
    }
    @Test(priority = 1)
    private void registerSuccess() throws JSONException {
        String email = RestUtils.gmailGenerator();
        String telephone = RestUtils.telephoneNumber();
        String name = RestUtils.nameGenerator();
        String password = RestUtils.passwordGenerator();

        register(name, email, telephone, password,password);

        setIdUser();
        writeFile(Integer.toString(getIdUser()),"idUser.txt");

        //checking response body
        checkBody("Registration is successful.");

        //checking status code
        checkDevStatus("001");
        checkStatusCode("201");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathRegis());

        //checking response data
        checkRegisData(name,email,telephone,Integer.toString(getIdUser()));

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 2)
    private void login() throws JSONException {
        String telephone = loadFile("telephone.txt");
        String password = loadFile("password.txt");

        login(telephone,password);

        setIdUser();
        writeFile(Integer.toString(getIdUser()),"idUser.txt");
        setToken();
        writeFile(getToken(),"token.txt");

        //checking response body
        checkBody("You are logged in.");

        //checking status code
        checkDevStatus("010");
        checkStatusCode("200");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathLogin());

        //checking response data
        checkLoginData(telephone,Integer.toString(getIdUser()));

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 3)
    private void editProfile(){
        String name = "empty";
        String email = this.email;
        String oldPassword = "empty";
        String newPassword = "empty";
        String confirmPassword = "empty";
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        editUser(name,email,oldPassword,newPassword,confirmPassword,idUser,token);
        writeFile(newPassword,"password.txt");
        //checking response body
        checkBody("Email already exists.");

        //checking status code
        checkDevStatus("007");
        checkStatusCode("400");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathEditProfile(idUser));

        //checking response data
        checkEmptyData();

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 3)
    private void logout(){
        String token = loadFile("token.txt");
        String idUser = loadFile("idUser.txt");
        logout(idUser,token);

        //checking response body
        checkBody("You are logged out.");

        //checking status code
        checkDevStatus("041");
        checkStatusCode("200");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathLogout(idUser));

        //checking response data
        checkEmptyData();

        //checking response time
        checkResponseTime("3000");
    }


}

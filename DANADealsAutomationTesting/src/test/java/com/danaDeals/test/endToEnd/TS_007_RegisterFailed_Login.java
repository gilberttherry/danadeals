package com.danaDeals.test.endToEnd;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.RestUtils;
import org.json.JSONException;
import org.testng.annotations.Test;

public class TS_007_RegisterFailed_Login extends TestBase {

    @Test(priority = 0)
    private void registerFailed() throws JSONException {
        String email = RestUtils.gmailGenerator();
        String telephone = RestUtils.telephoneNumber();
        String name = RestUtils.nameGenerator();
        String password = RestUtils.passwordGenerator();

        name +="%";

        register(name, email, telephone, password,password);

        //checking response body
        checkBody("Name is invalid.");

        //checking status code
        checkDevStatus("003");
        checkStatusCode("400");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathRegis());

        //checking response time
        checkResponseTime("3000");

        //checking empty data
        checkEmptyData();
    }

    @Test(priority = 1)
    private void loginFailed(){
        String telephone = loadFile("telephone.txt");
        String password = loadFile("password.txt");

        login(telephone,password);


        //checking response body
        checkBody("Your data do not match our records.");

        //checking status code
        checkDevStatus("011");
        checkStatusCode("404");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathLogin());

        //checking response time
        checkResponseTime("3000");

        //checking empty data
        checkEmptyData();
    }
}

package com.danaDeals.test.endToEnd;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.RestUtils;
import org.json.JSONException;
import org.testng.annotations.Test;

public class TS_052_Register_LoginFailed_CreateOrder extends TestBase {
    @Test(priority = 0)
    private void registerSuccess() throws JSONException {
        String email = RestUtils.gmailGenerator();
        String telephone = RestUtils.telephoneNumber();
        String name = RestUtils.nameGenerator();
        String password = RestUtils.passwordGenerator();

        register(name, email, telephone, password,password);

        setIdUser();
        writeFile(Integer.toString(getIdUser()),"idUser.txt");

        //checking response body
        checkBody("Registration is successful.");

        //checking status code
        checkDevStatus("001");
        checkStatusCode("201");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathRegis());

        //checking response data
        checkRegisData(name,email,telephone,Integer.toString(getIdUser()));

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 1)
    private void loginFailed() throws JSONException {
        String telephone = loadFile("telephone.txt");
        String password = RestUtils.changePassword1(loadFile("password.txt"));

        login(telephone,password);

        //checking response body
        checkBody("Your data do not match our records.");

        //checking status code
        checkDevStatus("011");
        checkStatusCode("404");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathLogin());

        //checking response data
        checkEmptyData();

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 3)
    private void createOrder() throws JSONException {
        String idVoucher = loadFile("unlimitedVoucher.txt");
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        createOrderVoucher(idVoucher,idUser,token);

        //check response body
        checkBody("You are not authorized.");

        //check status
        checkDevStatus("021");
        checkStatusCode("401");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathCreateOrder(idUser));

        //checking response data
        checkEmptyData();

        //check response time
        checkResponseTime("3000");

    }

}

package com.danaDeals.test.endToEnd;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.RestUtils;
import org.json.JSONException;
import org.testng.annotations.Test;

public class TS_113_Register_Login_CreateOrder_PayOrder_TopUpWallet extends TestBase {

    @Test(priority = 0)
    private void registerSuccess() throws JSONException {
        String email = RestUtils.gmailGenerator();
        String telephone = RestUtils.telephoneNumber();
        String name = RestUtils.nameGenerator();
        String password = RestUtils.passwordGenerator();

        register(name, email, telephone, password,password);

        setIdUser();
        writeFile(Integer.toString(getIdUser()),"idUser.txt");

        //checking response body
        checkBody("Registration is successful.");

        //checking status code
        checkDevStatus("001");
        checkStatusCode("201");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathRegis());

        //checking response data
        checkRegisData(name,email,telephone,Integer.toString(getIdUser()));
        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 1)
    private void login() throws JSONException {
        String telephone = loadFile("telephone.txt");
        String password = loadFile("password.txt");

        login(telephone,password);

        setIdUser();
        writeFile(Integer.toString(getIdUser()),"idUser.txt");
        setToken();
        writeFile(getToken(),"token.txt");

        //checking response body
        checkBody("You are logged in.");

        //checking status code
        checkDevStatus("010");
        checkStatusCode("200");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathLogin());

        //checking response data
        checkLoginData(telephone,Integer.toString(getIdUser()));

        //checking response time
        checkResponseTime("3000");

        setToken();
        writeFile(getToken(),"oldToken.txt");
    }

    @Test(priority = 2)
    private void createOrder() throws JSONException {
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");
        String idVoucher = "3";
        createOrderVoucher(idVoucher,idUser,token);

        //checking response body
        checkBody("A new transaction has been created!");

        //checking status code
        checkDevStatus("052");
        checkStatusCode("201");

        //checking response time
        checkResponseTime("3000");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathCreateOrder(idUser));

        //checking response data
        setIdTransaction();
        writeFile(Integer.toString(getIdTransaction()),"idTransaction.txt");
    }

    @Test(priority = 3)
    private void payOrder(){
        String idTransaction = loadFile("idTransaction.txt");
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        payOrderVoucher(idTransaction,idUser,token);

        //checking response body
        checkBody("Your balance is not enough.");

        //checking status code
        checkDevStatus("029");
        checkStatusCode("402");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathPayOrder(idUser));

        //checking response data
        checkEmptyData();

        //checking response time
        checkResponseTime("3000");

    }

    @Test(priority = 4)
    private void topUp(){
        String amount = "100000";
        String telephone = loadFile("telephone.txt");
        String virtualNumber = RestUtils.virtualNumberGenerator(telephone,"bni");
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        topUp(virtualNumber,amount,idUser,token);

        //check response body
        checkBody("Your TOPUP is successful.");

        //check status
        checkDevStatus("033");
        checkStatusCode("201");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathTopUp(idUser));

        //checking response data
        checkEmptyData();

        //check response time
        checkResponseTime("3000");

    }

}

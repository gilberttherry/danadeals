package com.danaDeals.test.endToEnd;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.RestUtils;
import org.json.JSONException;
import org.testng.annotations.Test;

public class TS_006_Login_Logout extends TestBase {


    @Test(priority = 1)
    private void login() throws JSONException {
        String telephone = RestUtils.telephoneNumber();
        String password = RestUtils.passwordGenerator();

        login(telephone,password);

        //checking response body
        checkBody("Your data do not match our records.");

        //checking status code
        checkDevStatus("011");
        checkStatusCode("404");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathLogin());

        //checking response time
        checkResponseTime("3000");

        //checking empty data
        checkEmptyData();
    }

    @Test(priority = 2)
    private void logout(){
        String token = RestUtils.tokenGenerator();

        String idUser = loadFile("idUser.txt");
        logout(idUser,token);
        //checking response body
        checkBody("You are not authorized.");

        //checking status code
        checkDevStatus("021");
        checkStatusCode("401");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathLogout(idUser));

        //checking response time
        checkResponseTime("3000");

        //checking empty data
        checkEmptyData();
    }
}

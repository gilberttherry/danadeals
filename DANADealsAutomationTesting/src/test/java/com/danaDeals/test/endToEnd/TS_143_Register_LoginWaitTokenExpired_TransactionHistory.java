package com.danaDeals.test.endToEnd;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.RestUtils;
import org.json.JSONException;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class TS_143_Register_LoginWaitTokenExpired_TransactionHistory extends TestBase {
    @Test(priority = 0)
    private void registerSuccess() throws JSONException {
        String email = RestUtils.gmailGenerator();
        String telephone = RestUtils.telephoneNumber();
        String name = RestUtils.nameGenerator();
        String password = RestUtils.passwordGenerator();

        register(name, email, telephone, password,password);

        setIdUser();
        writeFile(Integer.toString(getIdUser()),"idUser.txt");

        //checking response body
        checkBody("Registration is successful.");

        //checking status code
        checkDevStatus("001");
        checkStatusCode("201");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathRegis());

        //checking response data
        checkRegisData(name,email,telephone,Integer.toString(getIdUser()));

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 1)
    private void login() throws JSONException {
        String telephone = loadFile("telephone.txt");
        String password = loadFile("password.txt");

        login(telephone,password);

        setIdUser();
        writeFile(Integer.toString(getIdUser()),"idUser.txt");
        setToken();
        writeFile(getToken(),"token.txt");

        //checking response body
        checkBody("You are logged in.");

        //checking status code
        checkDevStatus("010");
        checkStatusCode("200");

        //checking timestamp
        checkTimestamp();

        //checking login data
        checkLoginData(telephone,Integer.toString(getIdUser()));

        //checking path
        checkPath(getPathLogin());

        //checking response time
        checkResponseTime("3000");
    }
    @Test(priority = 2)
    private void transactionHistory() throws InterruptedException {
        Thread.sleep(180000);
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");
        String category = "empty";
        String page = "0";
        DateTimeFormatter dtf = java.time.format.DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDateTime now = LocalDateTime.now();
        String date = dtf.format(now);

        transactionHistory(category,date,date,page,idUser,token);

        //checking response body
        checkBody("You are not authorized.");

        //checking status code
        checkDevStatus("021");
        checkStatusCode("401");

        //checking response time
        checkResponseTime("3000");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathTransactionHistory(idUser));

        //checking response data
        checkEmptyData();
    }
}

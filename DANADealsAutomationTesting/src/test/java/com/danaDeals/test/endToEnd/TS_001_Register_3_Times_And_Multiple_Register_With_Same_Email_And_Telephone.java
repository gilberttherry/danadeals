package com.danaDeals.test.endToEnd;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.RestUtils;
import org.json.JSONException;
import org.testng.annotations.Test;

public class TS_001_Register_3_Times_And_Multiple_Register_With_Same_Email_And_Telephone extends TestBase {
    private String telephone;
    @Test(priority = 0)
    private void registerWithDuplicateUniqueAttribute() throws JSONException {
        String email = RestUtils.gmailGenerator();
        String telephone = RestUtils.telephoneNumber();
        String name = RestUtils.nameGenerator();
        String password = RestUtils.passwordGenerator();
        this.telephone = telephone;

        register(name, email, telephone, password, password);
        setIdUser();
        writeFile(Integer.toString(getIdUser()), "idUser.txt");

        //checking response body
        checkBody("Registration is successful.");

        //checking status code
        checkDevStatus("001");
        checkStatusCode("201");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathRegis());

        //checking response data
        checkRegisData(name, email, telephone, Integer.toString(getIdUser()));

        //checking response time
        checkResponseTime("3000");

        //register with same email
        for (int i = 0; i < 4; i++) {
            if (i > 1) {
                email = email.toUpperCase();
                telephone = RestUtils.telephoneNumber();
                name = RestUtils.nameGenerator();
                password = RestUtils.passwordGenerator();
                if (i == 3)
                    email = email.toLowerCase();
            }
            register(name, email, telephone, password, password);

            //checking response body
            checkBody("Email already exists.");

            //checking status code
            checkDevStatus("007");
            checkStatusCode("400");

            //checking timestamp
            checkTimestamp();

            //checking path
            checkPath(getPathRegis());

            //checking empty data
            checkEmptyData();

            //checking response time
            checkResponseTime("3000");
        }

        email = RestUtils.gmailGenerator();
        telephone = RestUtils.changeTelephone(this.telephone);
        name = RestUtils.nameGenerator();
        password = RestUtils.passwordGenerator();

        register(name, email, telephone, password, password);

        //checking response body
        checkBody("Phone number already exists.");

        //checking status code
        checkDevStatus("008");
        checkStatusCode("400");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathRegis());

        //checking empty data
        checkEmptyData();

        //checking response time
        checkResponseTime("3000");
    }

}

package com.danaDeals.test.endToEnd;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.RestUtils;
import org.json.JSONException;
import org.testng.annotations.Test;

public class TS_030_Register_LoginFailed_EditProfile extends TestBase {

    @Test(priority = 0)
    private void registerSuccess() throws JSONException {
        String email = RestUtils.gmailGenerator();
        String telephone = RestUtils.telephoneNumber();
        String name = RestUtils.nameGenerator();
        String password = RestUtils.passwordGenerator();

        register(name, email, telephone, password,password);

        setIdUser();
        writeFile(Integer.toString(getIdUser()),"idUser.txt");

        //checking response body
        checkBody("Registration is successful.");

        //checking status code
        checkDevStatus("001");
        checkStatusCode("201");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathRegis());

        //checking response data
        checkRegisData(name,email,telephone,Integer.toString(getIdUser()));

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 1)
    private void login(){
        String telephone = loadFile("telephone.txt");
        String password = loadFile("password.txt").toLowerCase();

        login(telephone,password);

        //checking response body
        checkBody("Password is invalid.");

        //checking status code
        checkDevStatus("005");
        checkStatusCode("400");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathLogin());

        //checking response data
        checkEmptyData();

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 2)
    private void editProfile(){
        String name = RestUtils.nameGenerator();
        String email = RestUtils.gmailGenerator();
        String oldPassword = "empty";
        String newPassword = "empty";
        String confirmPassword = "empty";
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        editUser(name,email,oldPassword,newPassword,confirmPassword,idUser,token);

        //checking response body
        checkBody("You are not authorized.");

        //checking status code
        checkDevStatus("021");
        checkStatusCode("401");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathEditProfile(idUser));

        //checking response data
        checkEmptyData();

        //checking response time
        checkResponseTime("3000");
    }

}

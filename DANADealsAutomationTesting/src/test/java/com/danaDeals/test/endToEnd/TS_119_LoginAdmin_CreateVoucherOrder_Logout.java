package com.danaDeals.test.endToEnd;

import com.danaDeals.base.TestBase;
import org.json.JSONException;
import org.testng.annotations.Test;

public class TS_119_LoginAdmin_CreateVoucherOrder_Logout extends TestBase {

    @Test(priority = 0)
    private void loginAdmin() throws JSONException {

        String telephoneAdmin = "+6287800000000";
        String passwordAdmin = "P@ssw0rd";

        login(telephoneAdmin, passwordAdmin);

        setIdUser();
        writeFile(Integer.toString(getIdUser()), "idUser.txt");
        setToken();
        writeFile(getToken(), "adminToken.txt");

        //check response body
        checkBody("You are logged in.");
        checkPath("/api/auth/login");
        checkTimestamp();

        //check status
        checkDevStatus("010");
        checkStatusCode("200");

        //check response time
        checkResponseTime("3000");
    }

    @Test(priority = 1)
    private void createOrderFailed() {

        String idVoucher = "10";
        String idUser = loadFile("idUser.txt");
        String token = loadFile("adminToken.txt");

        createOrderVoucher(idVoucher, idUser, token);

        //check response body
        checkBody("You are not authorized.");
        checkPath("/api/user/"+idUser+"/transaction/voucher");
        checkTimestamp();

        //check status
        checkDevStatus("021");
        checkStatusCode("401");

        //check response time
        checkResponseTime("3000");
    }

    @Test(priority = 2)
    private void logoutAdmin() {

        String idUser = loadFile("idUser.txt");
        String token = loadFile("adminToken.txt");

        logout(idUser, token);

        //checking response body
        checkBody("You are logged out.");
        checkPath("/api/user/"+idUser+"/logout");
        checkTimestamp();

        //checking status code
        checkDevStatus("041");
        checkStatusCode("200");

        //checking response time
        checkResponseTime("3000");
    }
}

package com.danaDeals.test.member.editProfile;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.RestUtils;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TS_01_Edit_Profile_Success extends TestBase {

    @BeforeClass
    private void editProfile() throws InterruptedException{
        logger.info("********" +getClass().getName()+ "*********");
        Thread.sleep(5);
    }

    @Test(priority = 0)
    private void EditAllProfile() throws InterruptedException {
        String oldPassword = loadFile("password.txt");
        String name = RestUtils.nameGenerator();
        String email = RestUtils.gmailGenerator();
        String newPassword = RestUtils.passwordGenerator();
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");
        for(int i=0;i<9;i++) {
            if (i==0){
                //edit all
                oldPassword = loadFile("password.txt");
                name = RestUtils.nameGenerator();
                email = RestUtils.gmailGenerator();
                newPassword = RestUtils.passwordGenerator();
                idUser = loadFile("idUser.txt");
                token = loadFile("token.txt");
            }
            else if (i==1){
                //edit name and email
                oldPassword = "empty";
                name = RestUtils.nameGenerator();
                email = RestUtils.gmailGenerator();
                newPassword = "empty";
                idUser = loadFile("idUser.txt");
                token = loadFile("token.txt");
            }
            else if (i==2){
                //edit name and password
                oldPassword = loadFile("password.txt");
                name = RestUtils.nameGenerator();
                email = "empty";
                newPassword = RestUtils.passwordGenerator();
                idUser = loadFile("idUser.txt");
                token = loadFile("token.txt");
            }
            else if (i==3){
                //edit email and password
                oldPassword = loadFile("password.txt");
                name = "empty";
                email = RestUtils.gmailGenerator();
                newPassword = RestUtils.passwordGenerator();
                idUser = loadFile("idUser.txt");
                token = loadFile("token.txt");
            }
            else if (i==4){
                //edit name
                oldPassword = "empty";
                name = RestUtils.nameGenerator();
                email = "empty";
                newPassword = "empty";
                idUser = loadFile("idUser.txt");
                token = loadFile("token.txt");
            }else if (i==5){
                //edit email
                oldPassword = "empty";
                name = "empty";
                email = RestUtils.gmailGenerator();
                newPassword = "empty";
                idUser = loadFile("idUser.txt");
                token = loadFile("token.txt");

            }else if (i==6){
                //edit password
                oldPassword = loadFile("password.txt");
                name = "empty";
                email = "empty";
                newPassword = RestUtils.passwordGenerator();
                idUser = loadFile("idUser.txt");
                token = loadFile("token.txt");

            }else if (i==7){
                //edit same name
                oldPassword = "empty";
                name = loadFile("name.txt");
                email = "empty";
                newPassword = "empty";
                idUser = loadFile("idUser.txt");
                token = loadFile("token.txt");
            }else if (i==8){
                //edit same password
                oldPassword = loadFile("password.txt");
                name = "empty";
                email = "empty";
                newPassword = loadFile("password.txt");
                idUser = loadFile("idUser.txt");
                token = loadFile("token.txt");
            }
            editUser(name, email, oldPassword, newPassword, newPassword, idUser, token);

            //checking response body
            checkBody("Successfully edit profile data.");

            //checking status code
            checkDevStatus("023");
            checkStatusCode("200");

            //checking response time
            checkResponseTime("3000");

            //checking timestamp
            checkTimestamp();

            //check response data
            checkEditProfileData(name, email, newPassword);

            //checking path
            checkPath(getPathEditProfile(idUser));

            Thread.sleep(5);
        }
    }

}

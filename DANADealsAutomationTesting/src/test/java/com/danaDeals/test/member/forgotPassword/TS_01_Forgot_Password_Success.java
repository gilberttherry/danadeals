package com.danaDeals.test.member.forgotPassword;

import com.danaDeals.base.TestBase;
import org.json.JSONException;
import org.testng.annotations.Test;

public class TS_01_Forgot_Password_Success extends TestBase {

    @Test(priority = 0)
    private void forgotPasswordSamePassword() throws JSONException {
        logger.info("***** " + getClass().getName() + " *****");
        String idUser = loadFile("idUser.txt");
        String newPassword = loadFile("password.txt");

        forgotPassword(newPassword,newPassword,idUser);
        writeFile(newPassword,"newPassword.txt");

        //check response body
        checkBody("Change Password Success.");

        //check status
        checkDevStatus("019");
        checkStatusCode("200");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathForgotPassword(idUser));

        //checking empty data
        checkEmptyData();

        //check response time
        checkResponseTime("3000");

    }


}

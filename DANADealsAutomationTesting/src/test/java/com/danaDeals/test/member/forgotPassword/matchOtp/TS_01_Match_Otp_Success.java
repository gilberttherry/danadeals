package com.danaDeals.test.member.forgotPassword.matchOtp;

import com.danaDeals.base.TestBase;
import org.json.JSONException;
import org.testng.annotations.Test;

public class TS_01_Match_Otp_Success extends TestBase {

    @Test(priority = 0)
    private void matchOtpSuccess() throws JSONException {
        logger.info("***** " + getClass().getName() + " *****");
        String idUser = loadFile("idUser.txt");

        matchOtp("0000",idUser);

        //check response body
        checkBody("OTP Match.");

        //check status
        checkStatusCode("200");
        checkDevStatus("015");

        //check response time
        checkResponseTime("3000");

        //checking timestamp
        checkTimestamp();

        //checking empty data
        checkEmptyData();

        //checking path
        checkPath(getPathMatchOtp(idUser));
    }


}

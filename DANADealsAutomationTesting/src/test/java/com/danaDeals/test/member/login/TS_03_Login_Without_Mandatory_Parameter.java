package com.danaDeals.test.member.login;

import com.danaDeals.base.TestBase;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import org.json.simple.JSONObject;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class TS_03_Login_Without_Mandatory_Parameter extends TestBase {

    @BeforeClass
    private void prefix() throws InterruptedException {
        logger.info("********" + getClass().getName() + "*********");
        Thread.sleep(5);
    }

    @Test
    private void loginWithoutMandatoryParameter() {
        String password = loadFile("password.txt");
        String telephone = loadFile("telephone.txt");
        for(int i=0;i<7;i++) {
            RestAssured.baseURI = BaseURI;
            httpRequest = RestAssured.given();

            JSONObject requestParams = new JSONObject();
            if(i==0){
                requestParams.put("password", password);
            }
            else if(i==1){
                requestParams.put("telephone", telephone);
            }
            else if(i==2){
                requestParams.put("telephone", telephone);
                requestParams.put("password", null);
            }
            else if(i==3){
                requestParams.put("telephone", null);
                requestParams.put("password", password);
            }
            else if(i==4){
                requestParams.put("telephone", null);
                requestParams.put("password", null);
            }
            else if(i==5){
                requestParams.put("telephone", null);
            }
            else if(i==6){
                requestParams.put("password", null);
            }

            httpRequest.header("Content-Type", "application/json");
            httpRequest.body(requestParams.toJSONString());

            response = httpRequest.request(Method.POST, "/api/auth/login");
            responseBody = response.getBody().asString();

            //checking response body
            checkBody("Please fill in all the forms!");

            //checking data
            //checking status code
            checkDevStatus("002");
            checkStatusCode("400");

            //checking response time
            checkResponseTime("3000");

            //checking timestamp
            checkTimestamp();

            //checking path
            checkPath(getPathLogin());

            //checking empty data
            checkEmptyData();
        }
    }

}

package com.danaDeals.test.member.register;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.RestUtils;
import com.danaDeals.utilities.Utility;
import org.apache.poi.util.SystemOutLogger;
import org.json.JSONException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;

public class TS_01_Register_Success extends TestBase {

    @Test(priority = 1)
    private void registerSuccessForAutomation() throws InterruptedException, JSONException {
        String name = RestUtils.nameGenerator();
        String email = RestUtils.gmailGenerator();
        String phoneNumber = RestUtils.telephoneNumber();
        String password = RestUtils.passwordGenerator();

        register(name,email,phoneNumber,password,password);
        //checking response body
        checkBody("Registration is successful.");

        //checking status code
        checkDevStatus("001");
        checkStatusCode("201");

        //checking response time
        checkResponseTime("3000");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathRegis());

        setIdUser();
        writeFile(Integer.toString(getIdUser()),"idUser.txt");

        checkRegisData(name,email,phoneNumber,Integer.toString(getIdUser()));

        Thread.sleep(5);
    }

}

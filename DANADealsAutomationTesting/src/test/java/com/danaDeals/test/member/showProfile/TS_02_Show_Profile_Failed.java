package com.danaDeals.test.member.showProfile;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.RestUtils;
import com.danaDeals.utilities.Utility;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;

public class TS_02_Show_Profile_Failed extends TestBase {

    @BeforeClass
    private void showProfile() throws InterruptedException{
        logger.info("********" +getClass().getName()+ "*********");
        Thread.sleep(5);
    }

    @Test(priority = 0)
    private void showProfileFailedOldToken()
    {
        String token = loadFile("oldToken.txt");
        String idUser = loadFile("idUser.txt");
        for(int i=0;i<4;i++) {
            if(i==1){
                token=loadFile("token.txt");
                idUser = Integer.toString(Integer.parseInt(idUser)+1);
            }
            else if(i==2)
            {
                token=token.toLowerCase();
            }
            else{
                token=token.toUpperCase();
            }
            showProfile(idUser, token);

            //check response body
            checkBody("You are not authorized.");

            //check Status
            checkStatusCode("401");
            checkDevStatus("021");

            //check response body
            checkResponseTime("3000");
            //checking timestamp
            checkTimestamp();

            //checking path
            checkPath(getPathshowProfile(idUser));

            //checking empty data
            checkEmptyData();
        }
    }
}

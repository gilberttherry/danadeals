package com.danaDeals.test.member.editProfile;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.RestUtils;
import com.danaDeals.utilities.Utility;
import org.springframework.security.core.parameters.P;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;

public class TS_02_Edit_Profile_Failed extends TestBase {

    @BeforeClass
    private void editProfile() throws InterruptedException{
        logger.info("********" +getClass().getName()+ "*********");
    }

    @Test(priority = 0)
    private void EditSameEmail() throws InterruptedException {
        String oldPassword = "empty";
        String name = "empty";
        String email = loadFile("email.txt");
        String newPassword = "empty";
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");
        for(int i=0;i<2;i++) {

            if(i==1){
                 oldPassword = loadFile("password.txt");
                 name = loadFile("name.txt");
                 email = loadFile("email.txt");
                 newPassword = loadFile("password.txt");
                 idUser = loadFile("idUser.txt");
                 token = loadFile("token.txt");
            }

            editUser(name, email, oldPassword, newPassword, newPassword, idUser, token);

            //checking response body
            checkBody("Email already exists.");

            //checking status code
            checkDevStatus("007");
            checkStatusCode("400");

            //checking response time
            checkResponseTime("3000");

            //checking timestamp
            checkTimestamp();

            //check response data
            checkEmptyData();

            //checking path
            checkPath(getPathEditProfile(idUser));

            Thread.sleep(5);
        }
    }

    @Test(dataProvider = "dataEmailError", priority = 1)
    private void emailError(String name, String email, String newPassword, String confirmPassword) throws InterruptedException{
        String oldPassword = loadFile("password.txt");
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        editUser(name, email, oldPassword, newPassword, confirmPassword,idUser,token);

        //checking response body
        checkBody("Email is invalid.");

        //checking status code
        checkDevStatus("004");
        checkStatusCode("400");

        //checking response time
        checkResponseTime("3000");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathEditProfile(idUser));

        //checking empty data
        checkEmptyData();

        Thread.sleep(20);
    }

    @DataProvider(name="dataEmailError")
    private Object[][] getDataEmptyEmailError() throws IOException {
        String path = "src/test/java/com/danaDeals/test/member/editProfile/dataEmailError.xlsx";
        int rownum = Utility.getRowCount(path, "Sheet1");
        int colcount = Utility.getCellCount(path, "Sheet1",1);
        String dataEmployes [][] = new String[rownum][colcount];
        for(int i=1;i<=rownum;i++) {
            for(int j=0;j<colcount;j++) {
                dataEmployes[i-1][j] = Utility.getCellData(path,"Sheet1",i,j);
            }
        }
        return(dataEmployes);
    }

    @Test(dataProvider = "dataNameError", priority = 2)
    private void nameError(String name, String email, String newPassword, String confirmPassword) throws InterruptedException{
        String oldPassword = loadFile("password.txt");
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        editUser(name, email, oldPassword, newPassword, confirmPassword,idUser,token);

        //checking response body
        checkBody("Name is invalid.");

        //checking status code
        checkDevStatus("003");
        checkStatusCode("400");

        //checking response time
        checkResponseTime("3000");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathEditProfile(idUser));

        //checking empty data
        checkEmptyData();

        Thread.sleep(5);
    }

    @DataProvider(name="dataNameError")
    private Object[][] getDataEmptyNameError() throws IOException {
        String path = "src/test/java/com/danaDeals/test/member/editProfile/dataNameError.xlsx";
        int rownum = Utility.getRowCount(path, "Sheet1");
        int colcount = Utility.getCellCount(path, "Sheet1",1);
        String dataEmployes [][] = new String[rownum][colcount];
        for(int i=1;i<=rownum;i++) {
            for(int j=0;j<colcount;j++) {
                dataEmployes[i-1][j] = Utility.getCellData(path,"Sheet1",i,j);
            }
        }
        return(dataEmployes);
    }
    @Test(priority = 3)
    private void wrongOldPasswordLowercase() throws InterruptedException {
        String name = RestUtils.nameGenerator();
        String email = RestUtils.gmailGenerator();
        String oldPassword = loadFile("password.txt").toLowerCase();
        String newPassword = RestUtils.passwordGenerator();
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        editUser(name,email,oldPassword,newPassword,newPassword,idUser,token);

        //checking response body
        checkBody("Old password not match with user old password!");

        //checking status code
        checkDevStatus("050");
        checkStatusCode("400");

        //checking response time
        checkResponseTime("3000");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathEditProfile(idUser));

        //checking empty data
        checkEmptyData();

        Thread.sleep(5);
    }
    @Test(dataProvider = "dataPasswordMissmatch", priority = 4)
    private void passwordMissmatch(String name, String email, String newPassword, String confirmPassword) throws InterruptedException{
        String oldPassword = loadFile("password.txt");
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");
        email = RestUtils.gmailGenerator();
        editUser(name, email, oldPassword, newPassword, confirmPassword,idUser,token);

        //checking response body
        checkBody("Your new password and confirmation do not match.");

        //checking status code
        checkDevStatus("025");
        checkStatusCode("400");

        //checking response time
        checkResponseTime("3000");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathEditProfile(idUser));

        //checking empty data
        checkEmptyData();

        Thread.sleep(5);
    }

    @DataProvider(name="dataPasswordMissmatch")
    private Object[][] getDataEmptyPasswordMissmatch() throws IOException {
        String path = "src/test/java/com/danaDeals/test/member/editProfile/dataPasswordMissmatch.xlsx";
        int rownum = Utility.getRowCount(path, "Sheet1");
        int colcount = Utility.getCellCount(path, "Sheet1",1);
        String dataEmployes [][] = new String[rownum][colcount];
        for(int i=1;i<=rownum;i++) {
            for(int j=0;j<colcount;j++) {
                dataEmployes[i-1][j] = Utility.getCellData(path,"Sheet1",i,j);
            }
        }
        return(dataEmployes);
    }

    @Test(dataProvider = "dataPasswordInvalid", priority = 5)
    private void passwordInvalid(String name, String email, String newPassword, String confirmPassword) throws InterruptedException{
        String oldPassword = loadFile("password.txt");
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");
        email = RestUtils.gmailGenerator();

        editUser(name, email, oldPassword, newPassword, confirmPassword,idUser,token);


        //checking response body
        checkBody("Your new password is invalid.");

        //checking status code
        checkDevStatus("026");
        checkStatusCode("400");

        //checking response time
        checkResponseTime("3000");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathEditProfile(idUser));

        //checking empty data
        checkEmptyData();

        Thread.sleep(5);
    }

    @DataProvider(name="dataPasswordInvalid")
    private Object[][] getDataEmptyPasswordInvalid() throws IOException {
        String path = "src/test/java/com/danaDeals/test/member/editProfile/dataPasswordInvalid.xlsx";
        int rownum = Utility.getRowCount(path, "Sheet1");
        int colcount = Utility.getCellCount(path, "Sheet1",1);
        String dataEmployes [][] = new String[rownum][colcount];
        for(int i=1;i<=rownum;i++) {
            for(int j=0;j<colcount;j++) {
                dataEmployes[i-1][j] = Utility.getCellData(path,"Sheet1",i,j);
            }
        }
        return(dataEmployes);
    }

    @Test(priority = 6)
    private void changePasswordWithoutMandatory() throws InterruptedException {
        String name = "empty";
        String email = "empty";
        String oldPassword="";
        String newPassword="";
        String confirmPassword="";
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");
        for(int i=0;i<3;i++) {
            if (i == 0) {
                oldPassword = "empty";
                newPassword = RestUtils.passwordGenerator();
                confirmPassword = newPassword;
            } else if (i == 1) {
                oldPassword = loadFile("password.txt");
                newPassword = "empty";
                confirmPassword = RestUtils.passwordGenerator();
            } else {
                oldPassword = loadFile("password.txt");
                newPassword = RestUtils.passwordGenerator();
                confirmPassword = "empty";
            }

            editUser(name, email, oldPassword, newPassword, confirmPassword, idUser, token);

            //checking response body
            checkBody("Please fill in all the forms!");

            //checking status code
            checkDevStatus("002");
            checkStatusCode("400");

            //checking response time
            checkResponseTime("3000");

            //checking timestamp
            checkTimestamp();

            //checking path
            checkPath(getPathEditProfile(idUser));

            //checking empty data
            checkEmptyData();
        }
        Thread.sleep(5);
    }

    @Test(priority = 7)
    private void EditNotAuthorized(){
        String oldPassword = loadFile("password.txt");
        String name = RestUtils.nameGenerator();
        String email = RestUtils.gmailGenerator();
        String newPassword = RestUtils.passwordGenerator();
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");
        for(int i=0;i<4;i++)
        {
            if(i==0)
            {
                idUser = Integer.toString(Integer.parseInt(idUser) + 1);
            }
            else if(i==1){
                token=token.toLowerCase();
            }
            else if(i==2){
                token=loadFile("oldToken.txt");
            }
            else if(i==3){
                token=token.toUpperCase();
            }
            editUser(name,email,oldPassword,newPassword,newPassword,idUser,token);

            //checking response body
            checkBody("You are not authorized.");

            //checking status code
            checkDevStatus("021");
            checkStatusCode("401");

            //checking response time
            checkResponseTime("3000");

            //checking timestamp
            checkTimestamp();

            //checking path
            checkPath(getPathEditProfile(idUser));

            //checking empty data
            checkEmptyData();
        }
    }

    @Test(priority = 8)
    private void wrongOldPasswordNotMatch() throws InterruptedException {
        String name = RestUtils.nameGenerator();
        String email = RestUtils.gmailGenerator();
        String oldPassword = loadFile("password.txt").toUpperCase();
        String newPassword = RestUtils.passwordGenerator();
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");
        for(int i=0;i<4;i++) {
            if (i == 0) {
                email = RestUtils.gmailGenerator();
                oldPassword = oldPassword.toLowerCase();
            }
            else if (i == 1) {
                email = RestUtils.gmailGenerator();
                oldPassword = oldPassword.toUpperCase();
            }
            else if(i==2){
                email = RestUtils.gmailGenerator();
                oldPassword = RestUtils.changePassword1(oldPassword);
            }
            else if(i==3){
                email = RestUtils.gmailGenerator();
                oldPassword = RestUtils.changePassword2(oldPassword);
            }
            editUser(name, email, oldPassword, newPassword, newPassword, idUser, token);

            //checking response body
            checkBody("Old password not match with user old password!");

            //checking status code
            checkDevStatus("050");
            checkStatusCode("400");

            //checking response time
            checkResponseTime("3000");

            //checking timestamp
            checkTimestamp();

            //checking path
            checkPath(getPathEditProfile(idUser));

            //checking empty data
            checkEmptyData();
        }
        Thread.sleep(5);
    }

}

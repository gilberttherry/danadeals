package com.danaDeals.test.member.register;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.RestUtils;
import io.restassured.RestAssured;
import org.json.JSONException;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TS_04_Register_With_Duplicate_Attribute extends TestBase {
    String name = RestUtils.nameGenerator();
    String email = RestUtils.gmailGenerator();
    String phoneNumber = RestUtils.telephoneNumber();
    String password = RestUtils.passwordGenerator();
    @BeforeClass
    private void prefix() throws InterruptedException {
        logger.info("********" + getClass().getName() + "*********");
        Thread.sleep(5);
    }

    @Test(priority = 1)
    private void registerSuccessForAutomation() throws InterruptedException, JSONException {

        register(name,email,phoneNumber,password,password);
        setIdUser();
        //checking response body
        checkBody("Registration is successful.");

        //checking status code
        checkDevStatus("001");
        checkStatusCode("201");

        //checking response time
        checkResponseTime("3000");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathRegis());

        //checking response data
        checkRegisData(name,email,phoneNumber,Integer.toString(getIdUser()));

        Thread.sleep(5);
    }

    @Test(priority = 2)
    private void duplicatedEmail() throws InterruptedException, JSONException {
        RestAssured.baseURI = BaseURI;
        httpRequest = RestAssured.given();

        String telephone = RestUtils.telephoneNumber();
        String name = RestUtils.nameGenerator();
        String password = RestUtils.passwordGenerator();

        register(name, this.email, telephone, password, password);

        //checking response body
        checkBody("Email already exists.");

        //checking status code
        checkDevStatus("007");
        checkStatusCode("400");

        //checking response time
        checkResponseTime("3000");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathRegis());

        //checking response data
        checkEmptyData();

        Thread.sleep(5);
    }

    @Test(priority = 3)
    private void duplicatedName() throws InterruptedException, JSONException {
        RestAssured.baseURI = BaseURI;
        httpRequest = RestAssured.given();

        String email = RestUtils.gmailGenerator();
        String telephone = RestUtils.telephoneNumber();
        String password = RestUtils.passwordGenerator();

        register(this.name, email, telephone, password, password);
        setIdUser();
        //checking response body
        checkBody("Registration is successful.");

        //checking status code
        checkDevStatus("001");
        checkStatusCode("201");

        //checking response time
        checkResponseTime("3000");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathRegis());

        //checking response data
        checkRegisData(name,email,telephone,Integer.toString(getIdUser()));

        Thread.sleep(5);
    }

    @Test(priority = 4)
    private void duplicatedTelephone() throws InterruptedException, JSONException {
        RestAssured.baseURI = BaseURI;
        httpRequest = RestAssured.given();

        String email = RestUtils.gmailGenerator();
        String name = RestUtils.nameGenerator();
        String password = RestUtils.passwordGenerator();

        register(name, email, this.phoneNumber, password, password);

        //checking response body
        checkBody("Phone number already exists.");

        //checking status code
        checkDevStatus("008");
        checkStatusCode("400");

        //checking response time
        checkResponseTime("3000");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathRegis());

        //checking response data
        checkEmptyData();

        Thread.sleep(5);
    }


    @Test(priority = 6)
    private void duplicatedEmailLowercase() throws InterruptedException, JSONException {
        RestAssured.baseURI = BaseURI;
        httpRequest = RestAssured.given();

        String email = this.email.toLowerCase();
        String telephone = RestUtils.telephoneNumber();
        String name = RestUtils.nameGenerator();
        String password = RestUtils.passwordGenerator();

        register(name, email, telephone, password, password);

        //checking response body
        checkBody("Email already exists.");

        //checking status code
        checkDevStatus("007");
        checkStatusCode("400");

        //checking response time
        checkResponseTime("3000");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathRegis());

        //checking response data
        checkEmptyData();

        Thread.sleep(5);
    }

    @Test(priority = 7)
    private void duplicatedEmaiUppercase() throws InterruptedException, JSONException {
        RestAssured.baseURI = BaseURI;
        httpRequest = RestAssured.given();

        String email = this.email.toUpperCase();
        String telephone = RestUtils.telephoneNumber();
        String name = RestUtils.nameGenerator();
        String password = RestUtils.passwordGenerator();

        register(name, email, telephone, password, password);

        //checking response body
        checkBody("Email already exists.");

        //checking status code
        checkDevStatus("007");
        checkStatusCode("400");

        //checking response time
        checkResponseTime("3000");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathRegis());

        //checking response data
        checkEmptyData();

        Thread.sleep(5);
    }

    @Test(priority = 8)
    private void duplicatedPassword() throws InterruptedException, JSONException {
        RestAssured.baseURI = BaseURI;
        httpRequest = RestAssured.given();

        String email = RestUtils.gmailGenerator();
        String telephone = RestUtils.telephoneNumber();
        String name = RestUtils.nameGenerator();

        register(name, email, telephone, this.password, this.password);
        setIdUser();

        //checking response body
        checkBody("Registration is successful.");

        //checking status code
        checkDevStatus("001");
        checkStatusCode("201");

        //checking response time
        checkResponseTime("3000");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathRegis());

        //checking response data
        checkRegisData(name,email,telephone,Integer.toString(getIdUser()));

        Thread.sleep(5);
    }
}


package com.danaDeals.test.member.showProfile;

import com.danaDeals.base.TestBase;
import org.json.JSONException;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TS_01_Show_Profile_Success extends TestBase {

    @BeforeClass
    private void showProfile() throws InterruptedException{
        logger.info("********" +getClass().getName()+ "*********");
        Thread.sleep(5);
    }

    @Test(priority = 0)
    private void topUpSuccess() throws JSONException {
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        showProfile(idUser,token);

        //check response body
        checkBody("Successfully display profile data.");

        //check Status
        checkStatusCode("200");
        checkDevStatus("020");

        //check response body
        checkResponseTime("3000");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathshowProfile(idUser));

        //checking show profile
        checkShowProfileData();
    }

}

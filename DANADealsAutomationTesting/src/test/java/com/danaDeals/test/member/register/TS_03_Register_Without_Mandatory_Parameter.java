package com.danaDeals.test.member.register;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.RestUtils;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import org.json.simple.JSONObject;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TS_03_Register_Without_Mandatory_Parameter extends TestBase {
    @BeforeClass
    private void prefix() {
        logger.info("***** " + getClass().getName() + " (Name) *****");
    }

    @Test
    private void registerWithoutMandatoryParameter() {
        String name = RestUtils.nameGenerator();
        String email = RestUtils.gmailGenerator();
        String telephone = RestUtils.telephoneNumber();
        String password = RestUtils.passwordGenerator();
        for (int i = 0; i < 10; i++) {
            RestAssured.baseURI = BaseURI;
            httpRequest = RestAssured.given();

            JSONObject requestParams = new JSONObject();
            if (i == 0) {
                requestParams.put("email", email);
                requestParams.put("telephone", telephone);
                requestParams.put("password", password);
                requestParams.put("retryPassword", password);
            } else if (i == 1) {
                requestParams.put("name", name);
                requestParams.put("telephone", telephone);
                requestParams.put("password", password);
                requestParams.put("retryPassword", password);
            } else if (i == 2) {
                requestParams.put("name", name);
                requestParams.put("email", email);
                requestParams.put("password", password);
                requestParams.put("retryPassword", password);
            } else if (i == 3) {
                requestParams.put("name", name);
                requestParams.put("email", email);
                requestParams.put("telephone", telephone);
                requestParams.put("retryPassword", password);
            } else if (i == 4) {
                requestParams.put("name", name);
                requestParams.put("email", email);
                requestParams.put("telephone", telephone);
                requestParams.put("password", password);
            } else if (i == 5) {
                requestParams.put("name", null);
                requestParams.put("email", email);
                requestParams.put("telephone", telephone);
                requestParams.put("password", password);
                requestParams.put("retryPassword", password);
            } else if (i == 6) {
                requestParams.put("name", name);
                requestParams.put("email", null);
                requestParams.put("telephone", telephone);
                requestParams.put("password", password);
                requestParams.put("retryPassword", password);
            } else if (i == 7) {
                requestParams.put("name", name);
                requestParams.put("email", email);
                requestParams.put("telephone", null);
                requestParams.put("password", password);
                requestParams.put("retryPassword", password);
            } else if (i == 8) {
                requestParams.put("name", name);
                requestParams.put("email", email);
                requestParams.put("telephone", telephone);
                requestParams.put("password", null);
                requestParams.put("retryPassword", password);
            } else if (i == 9) {
                requestParams.put("name", name);
                requestParams.put("email", email);
                requestParams.put("telephone", telephone);
                requestParams.put("password", password);
                requestParams.put("retryPassword", null);
            }

            httpRequest.header("Content-Type", "application/json");
            httpRequest.body(requestParams.toJSONString());

            response = httpRequest.request(Method.POST, "/api/auth/register");
            responseBody = response.getBody().asString();

            //checking body
            checkBody("Please fill in all the forms!");

            //checking status code
            checkDevStatus("002");
            checkStatusCode("400");

            //checking empty data
            checkEmptyData();

            //checking response time
            checkResponseTime("3000");

            //checking timestamp
            checkTimestamp();

            //checking path
            checkPath(getPathRegis());
        }
    }
}

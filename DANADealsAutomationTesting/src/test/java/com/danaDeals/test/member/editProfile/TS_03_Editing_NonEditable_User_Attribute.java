package com.danaDeals.test.member.editProfile;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.RestUtils;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import org.testng.annotations.Test;

public class TS_03_Editing_NonEditable_User_Attribute extends TestBase {

    @Test(priority = 0)
    private void EditAllProfile(){
        RestAssured.baseURI = BaseURI;
        httpRequest = RestAssured.given();
        String telephone = RestUtils.telephoneNumber();
        String id = loadFile("idUser.txt");
        String token = loadFile("token.txt");
        httpRequest.header("Authorization", "Bearer " + token);

        requestParams.put("telephone", telephone);

        httpRequest.header("Content-Type", "application/json");
        httpRequest.body(requestParams.toJSONString());

        response = httpRequest.request(Method.PUT, "/api/user/"+id);
        responseBody = response.getBody().asString();

        //checking response body
        checkBody("Please fill in at least one field!");

        //checking status code
        checkDevStatus("024");
        checkStatusCode("400");

        //checking response time
        checkResponseTime("3000");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathEditProfile(id));

        //checking empty data
        checkEmptyData();
    }
}

package com.danaDeals.test.member.forgotPassword.requestOtp;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.RestUtils;
import com.danaDeals.utilities.Utility;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;

public class TS_02_Request_Otp_Failed extends TestBase {

    @BeforeClass
    private void postRequestOtp() throws InterruptedException{
        logger.info("********" +getClass().getName()+ "*********");
        Thread.sleep(5);
    }
    @Test(dataProvider = "dataTelephoneInvalid", priority = 0)
    private void dataDrivenRequestOtpTelephoneInvalid(String telephone, String password) throws InterruptedException{

        requestOtp(telephone);

        //checking response body
        checkBody("Phone number is invalid.");

        //checking status code
        checkDevStatus("006");
        checkStatusCode("400");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathRequestOtp());

        //checking response time
        checkResponseTime("3000");

        //checking response
        checkEmptyData();

        Thread.sleep(5);
    }

    @DataProvider(name="dataTelephoneInvalid")
    private Object[][] getDataEmptyTelephoneInvalid() throws IOException {
        String path = "src/test/java/com/danaDeals/test/member/forgotPassword/requestOtp/dataTelephoneInvalid.xlsx";
        int rownum = Utility.getRowCount(path, "Sheet1");
        int colcount = Utility.getCellCount(path, "Sheet1",1);
        String dataEmployes [][] = new String[rownum][colcount];
        for(int i=1;i<=rownum;i++) {
            for(int j=0;j<colcount;j++) {
                dataEmployes[i-1][j] = Utility.getCellData(path,"Sheet1",i,j);
            }
        }
        return(dataEmployes);
    }

    @Test(priority = 1)
    private void requestOtpTelephoneNotExist(){
        String telephone = RestUtils.telephoneNumber();
        requestOtp(telephone);

        //checking response body
        checkBody("Phone number does not exist.");

        //checking status code
        checkDevStatus("013");
        checkStatusCode("404");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathRequestOtp());

        //checking response
        checkEmptyData();

        //checking response time
        checkResponseTime("3000");
    }
}

package com.danaDeals.test.member.register;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.RestUtils;
import com.danaDeals.utilities.Utility;
import org.testng.annotations.*;

import java.io.IOException;

public class TS_02_Register_Failed extends TestBase {

    @BeforeClass
    private void postRegisterData() throws InterruptedException{
        logger.info("********" +getClass().getName()+ "*********");
    }

    @Test(dataProvider = "dataRegisterNameError", priority = 0)
    private void nameError(String name, String email, String telephone, String password, String retryPassword) throws InterruptedException{
        telephone = RestUtils.telephoneNumber();
        email = RestUtils.gmailGenerator();
        register(name, email, telephone, password, retryPassword);


        //checking response body
        checkBody("Name is invalid.");

        //checking status code
        checkDevStatus("003");
        checkStatusCode("400");

        //checking response time
        checkResponseTime("3000");

        //checking empty data
        checkEmptyData();

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathRegis());

        Thread.sleep(5);
    }

    @DataProvider(name="dataRegisterNameError")
    private Object[][] getDataEmptyNameError() throws  IOException {
        String path = "src/test/java/com/danaDeals/test/member/register/dataRegisterNameError.xlsx";
        int rownum = Utility.getRowCount(path, "Sheet1");
        int colcount = Utility.getCellCount(path, "Sheet1",1);
        String dataEmployes [][] = new String[rownum][colcount];
        for(int i=1;i<=rownum;i++) {
            for(int j=0;j<colcount;j++) {
                dataEmployes[i-1][j] = Utility.getCellData(path,"Sheet1",i,j);
            }
        }
        return(dataEmployes);
    }

    @Test(dataProvider = "dataRegisterEmailError", priority = 1)
    private void emailError (String name, String email, String telephone, String password, String retryPassword) throws InterruptedException{
        telephone = RestUtils.telephoneNumber();
        register(name, email, telephone, password, retryPassword);

        //checking response body
        checkBody("Email is invalid.");

        //checking status code
        checkDevStatus("004");
        checkStatusCode("400");

        //checking response time
        checkResponseTime("3000");

        //checking empty data
        checkEmptyData();

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathRegis());

        Thread.sleep(5);
    }

    @DataProvider(name="dataRegisterEmailError")
    private Object[][] getDataEmptyEmailError() throws  IOException {
        String path = "src/test/java/com/danaDeals/test/member/register/dataRegisterEmailError.xlsx";
        int rownum = Utility.getRowCount(path, "Sheet1");
        int colcount = Utility.getCellCount(path, "Sheet1",1);
        String dataEmployes [][] = new String[rownum][colcount];
        for(int i=1;i<=rownum;i++) {
            for(int j=0;j<colcount;j++) {
                dataEmployes[i-1][j] = Utility.getCellData(path,"Sheet1",i,j);
            }
        }
        return(dataEmployes);
    }

    @Test(dataProvider = "dataRegisterTelephoneError", priority = 2)
    private void telephoneError (String name, String email, String telephone, String password, String retryPassword) throws InterruptedException{

        register(name, email, telephone, password, retryPassword);
        //checking response body
        checkBody("Phone number is invalid.");
        //checking status code
        checkDevStatus("006");
        checkStatusCode("400");

        //checking response time
        checkResponseTime("3000");

        //checking empty data
        checkEmptyData();

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathRegis());

        Thread.sleep(5);
    }

    @DataProvider(name="dataRegisterTelephoneError")
    private Object[][] getDataEmptyTelephoneError() throws  IOException {
        String path = "src/test/java/com/danaDeals/test/member/register/dataRegisterTelephoneError.xlsx";
        int rownum = Utility.getRowCount(path, "Sheet1");
        int colcount = Utility.getCellCount(path, "Sheet1",1);
        String dataEmployes [][] = new String[rownum][colcount];
        for(int i=1;i<=rownum;i++) {
            for(int j=0;j<colcount;j++) {
                dataEmployes[i-1][j] = Utility.getCellData(path,"Sheet1",i,j);
            }
        }
        return(dataEmployes);
    }

    @Test(dataProvider = "dataRegisterPasswordInvalid", priority = 3)
    private void passwordInvalid (String password, String retryPassword) throws InterruptedException{
        String name = RestUtils.nameGenerator();
        String email = RestUtils.gmailGenerator();
        String telephone = RestUtils.telephoneNumber();

        register(name, email, telephone, password, retryPassword);

        //checking response body
        checkBody("Password is invalid.");

        //checking status code
        checkDevStatus("005");
        checkStatusCode("400");

        //checking response time
        checkResponseTime("3000");

        //checking empty data
        checkEmptyData();

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathRegis());

        Thread.sleep(5);
    }

    @DataProvider(name="dataRegisterPasswordInvalid")
    private Object[][] getDataEmptyPasswordInvalid() throws  IOException {
        String path = "src/test/java/com/danaDeals/test/member/register/dateRegisterPasswordInvalid.xlsx";
        int rownum = Utility.getRowCount(path, "Sheet1");
        int colcount = Utility.getCellCount(path, "Sheet1",1);
        String dataEmployes [][] = new String[rownum][colcount];
        for(int i=1;i<=rownum;i++) {
            for(int j=0;j<colcount;j++) {
                dataEmployes[i-1][j] = Utility.getCellData(path,"Sheet1",i,j);
            }
        }
        return(dataEmployes);
    }

    @Test(dataProvider = "dataRegisterPasswordMissmatch", priority = 4)
    private void passwordMissmatch (String name, String email, String telephone, String password, String retryPassword) throws InterruptedException{
        email=RestUtils.gmailGenerator();
        telephone = RestUtils.telephoneNumber();
        register(name, email, telephone, password, retryPassword);

        //checking response body
        checkBody("Password is missed match.");
        //checking status code
        checkDevStatus("009");
        checkStatusCode("400");

        //checking response time
        checkResponseTime("3000");

        //checking empty data
        checkEmptyData();

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathRegis());

        Thread.sleep(5);
    }

    @DataProvider(name="dataRegisterPasswordMissmatch")
    private Object[][] getDataEmptyPasswordMissmatch() throws  IOException {
        String path = "src/test/java/com/danaDeals/test/member/register/dataRegisterPasswordMissmatch.xlsx";
        int rownum = Utility.getRowCount(path, "Sheet1");
        int colcount = Utility.getCellCount(path, "Sheet1",1);
        String dataEmployes [][] = new String[rownum][colcount];
        for(int i=1;i<=rownum;i++) {
            for(int j=0;j<colcount;j++) {
                dataEmployes[i-1][j] = Utility.getCellData(path,"Sheet1",i,j);
            }
        }
        return(dataEmployes);
    }

}

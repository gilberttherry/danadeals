package com.danaDeals.test.member.forgotPassword;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.Utility;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;

public class TS_02_Forgot_Password_Failed extends TestBase {

    @BeforeClass
    private void postRequestOtp() throws InterruptedException{
        logger.info("********" +getClass().getName()+ "*********");
        Thread.sleep(5);
    }
    @Test(dataProvider = "dataPasswordInvalid", priority = 0)
    private void dataDrivenRequestOtpPasswordInvalid(String password, String confirmPassword) throws InterruptedException{
        String idUser = loadFile("idUser.txt");

        forgotPassword(password,confirmPassword,idUser);

        //checking response body
        checkBody("Password is invalid.");

        //checking status code
        checkDevStatus("005");
        checkStatusCode("400");

        //checking response time
        checkResponseTime("3000");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathForgotPassword(idUser));

        //checking empty data
        checkEmptyData();

        Thread.sleep(5);
    }

    @DataProvider(name="dataPasswordInvalid")
    private Object[][] getDataEmptyPasswordInvalid() throws IOException {
        String path = "src/test/java/com/danaDeals/test/member/forgotPassword/dataPasswordInvalid.xlsx";
        int rownum = Utility.getRowCount(path, "Sheet1");
        int colcount = Utility.getCellCount(path, "Sheet1",1);
        String dataEmployes [][] = new String[rownum][colcount];
        for(int i=1;i<=rownum;i++) {
            for(int j=0;j<colcount;j++) {
                dataEmployes[i-1][j] = Utility.getCellData(path,"Sheet1",i,j);
            }
        }
        return(dataEmployes);
    }
    @Test(dataProvider = "dataPasswordNotMatch", priority = 0)
    private void dataDrivenRequestOtpPasswordNotMatch(String password, String confirmPassword) throws InterruptedException{
        String idUser = loadFile("idUser.txt");

        forgotPassword(password,confirmPassword,idUser);

        //checking response body
        checkBody("Password is missed match.");

        //checking status code
        checkDevStatus("009");
        checkStatusCode("400");

        //checking response time
        checkResponseTime("3000");

        //checking timestamp
        checkTimestamp();

        //checking empty data
        checkEmptyData();

        //checking path
        checkPath(getPathForgotPassword(idUser));

        Thread.sleep(5);
    }

    @DataProvider(name="dataPasswordNotMatch")
    private Object[][] getDataEmptyPasswordNotMatch() throws IOException {
        String path = "src/test/java/com/danaDeals/test/member/forgotPassword/dataPasswordNotMatch.xlsx";
        int rownum = Utility.getRowCount(path, "Sheet1");
        int colcount = Utility.getCellCount(path, "Sheet1",1);
        String dataEmployes [][] = new String[rownum][colcount];
        for(int i=1;i<=rownum;i++) {
            for(int j=0;j<colcount;j++) {
                dataEmployes[i-1][j] = Utility.getCellData(path,"Sheet1",i,j);
            }
        }
        return(dataEmployes);
    }

}

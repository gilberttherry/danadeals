package com.danaDeals.test.member.editProfile;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.RestUtils;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import org.testng.annotations.Test;

public class TS_04_Edit_Profile_Without_Any_Parameter extends TestBase {
    @Test(priority = 0)
    private void editProfileWithoutAnyParameter(){
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");
        for(int i=0;i<2;i++) {

            RestAssured.baseURI = BaseURI;
            httpRequest = RestAssured.given();
            httpRequest.header("Authorization", "Bearer " + token);

            httpRequest.header("Content-Type", "application/json");
            httpRequest.body(requestParams.toJSONString());
            if(i==1){
                requestParams.put("name", null);
                requestParams.put("email", null);
                requestParams.put("oldPassword", null);
                requestParams.put("newPassword", null);
                requestParams.put("confirmPassword", null);
            }
            response = httpRequest.request(Method.PUT, "/api/user/" + idUser);
            responseBody = response.getBody().asString();

            //checking response body
            checkBody("Please fill in at least one field!");

            //checking status code
            checkDevStatus("024");
            checkStatusCode("400");

            //checking response time
            checkResponseTime("3000");

            //checking timestamp
            checkTimestamp();

            //checking path
            checkPath(getPathEditProfile(idUser));

            //checking empty data
            checkEmptyData();
        }
    }
}

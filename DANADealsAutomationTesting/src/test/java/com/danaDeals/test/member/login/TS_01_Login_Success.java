package com.danaDeals.test.member.login;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.RestUtils;
import org.json.JSONException;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class TS_01_Login_Success extends TestBase {

    @BeforeClass
    private void postLoginData() throws InterruptedException{
        logger.info("********" +getClass().getName()+ "*********");
    }

    @Test(priority = 0)
    private void loginSuccess() throws JSONException {
        String telephone = loadFile("telephone.txt");
        String password = loadFile("password.txt");
        telephone = RestUtils.changeTelephone(telephone);

        login(telephone, password);
        setIdUser();
        writeFile(Integer.toString(getIdUser()),"idUser.txt");
        setToken();
        writeFile(getToken(),"oldToken.txt");


        //checking response body
        checkBody("You are logged in.");

        //checking status code
        checkDevStatus("010");
        checkStatusCode("200");

        //checking login data
        checkLoginData(telephone,Integer.toString(getIdUser()));

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathLogin());

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 1)
    private void loginSuccessSecondTimes() throws JSONException {
        String telephone = loadFile("telephone.txt");
        String password = loadFile("password.txt");
        telephone = RestUtils.changeTelephone(telephone);

        login(telephone, password);

        //checking idUser
        setIdUser();
        Assert.assertEquals(Integer.parseInt(loadFile("idUser.txt")),getIdUser());
        writeFile(Integer.toString(getIdUser()),"idUser.txt");

        //checking token
        setToken();
        Assert.assertTrue(!(getToken().equals(loadFile("oldToken.txt"))));
        writeFile(getToken(),"token.txt");


        //checking response body
        checkBody("You login in another device.");

        //checking status code
        checkDevStatus("047");
        checkStatusCode("200");

        //checking login data
        checkLoginData(telephone,Integer.toString(getIdUser()));

        //checking response time
        checkResponseTime("3000");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathLogin());

    }

}

package com.danaDeals.test.member.forgotPassword.requestOtp;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.RestUtils;
import org.json.JSONException;
import org.testng.annotations.Test;

public class TS_01_Request_Otp_Success extends TestBase {

    @Test(priority = 0)
    private void requestOtpSuccess() throws JSONException {
        logger.info("***** " + getClass().getName() + " *****");
        String telephone = loadFile("telephone.txt");
        for(int i=0;i<2;i++) {
            if(i==1){
                telephone=RestUtils.changeTelephone(telephone);
            }
            requestOtp(telephone);

            //check response body
            checkBody("Your OTP has sent to your phone number.");

            //check response data
            setIdUserReqOtp();
            writeFile(Integer.toString(getIdUser()), "idUser.txt");

            //check status
            checkDevStatus("012");
            checkStatusCode("200");

            //checking timestamp
            checkTimestamp();

            //checking path
            checkPath(getPathRequestOtp());

            //check response time
            checkResponseTime("3000");
        }

    }
}


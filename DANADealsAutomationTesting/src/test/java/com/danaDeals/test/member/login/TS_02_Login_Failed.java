package com.danaDeals.test.member.login;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.RestUtils;
import com.danaDeals.utilities.Utility;
import org.testng.annotations.*;

import java.io.IOException;

public class TS_02_Login_Failed extends TestBase {

    @BeforeClass
    private void postLoginData() throws InterruptedException{
        logger.info("********" +getClass().getName()+ "*********");
        Thread.sleep(5);
    }
    @Test(dataProvider = "datalogin", priority = 0)
    private void dataDrivenPostLogin(String telephone, String password) throws InterruptedException{
        telephone = RestUtils.telephoneNumber();
        password = RestUtils.passwordGenerator();
        login(telephone, password);

        //checking response body
        checkBody("Your data do not match our records.");

        //checking status code
        checkDevStatus("011");
        checkStatusCode("404");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathLogin());

        //checking response time
        checkResponseTime("3000");

        //checking empty data
        checkEmptyData();

        Thread.sleep(5);
    }

    @DataProvider(name="datalogin")
    private Object[][] getDataEmpty() throws  IOException {
        String path = "src/test/java/com/danaDeals/test/member/login/datalogin.xlsx";
        int rownum = Utility.getRowCount(path, "Sheet1");
        int colcount = Utility.getCellCount(path, "Sheet1",1);
        String dataEmployes [][] = new String[rownum][colcount];
        for(int i=1;i<=rownum;i++) {
            for(int j=0;j<colcount;j++) {
                dataEmployes[i-1][j] = Utility.getCellData(path,"Sheet1",i,j);
            }
        }
        return(dataEmployes);
    }

    @Test(dataProvider = "dataloginTelephoneError", priority = 1)
    private void dataDrivenPostLoginTelephoneError(String telephone, String password) throws InterruptedException{
        password = loadFile("password.txt");
        login(telephone, password);

        //checking response body
        checkBody("Phone number is invalid.");

        //checking status code
        checkDevStatus("006");
        checkStatusCode("400");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathLogin());

        //checking response time
        checkResponseTime("3000");

        //checking empty data
        checkEmptyData();

        Thread.sleep(5);
    }

    @DataProvider(name="dataloginTelephoneError")
    private Object[][] getDataEmptyTelephoneError() throws  IOException {
        String path = "src/test/java/com/danaDeals/test/member/login/dataloginTelephoneError.xlsx";
        int rownum = Utility.getRowCount(path, "Sheet1");
        int colcount = Utility.getCellCount(path, "Sheet1",1);
        String dataEmployes [][] = new String[rownum][colcount];
        for(int i=1;i<=rownum;i++) {
            for(int j=0;j<colcount;j++) {
                dataEmployes[i-1][j] = Utility.getCellData(path,"Sheet1",i,j);
            }
        }
        return(dataEmployes);
    }

    @Test(dataProvider = "dataloginPasswordError", priority = 2)
    private void dataDrivenPostLoginPasswordError(String telephone, String password) throws InterruptedException{
        telephone = loadFile("telephone.txt");
        login(telephone, password);

        //checking response body
        checkBody("Password is invalid.");

        //checking status code
        checkDevStatus("005");
        checkStatusCode("400");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathLogin());

        //checking response time
        checkResponseTime("3000");

        //checking empty data
        checkEmptyData();

        Thread.sleep(5);
    }

    @DataProvider(name="dataloginPasswordError")
    private Object[][] getDataEmptyPasswordError() throws  IOException {
        String path = "src/test/java/com/danaDeals/test/member/login/dataloginPasswordError.xlsx";
        int rownum = Utility.getRowCount(path, "Sheet1");
        int colcount = Utility.getCellCount(path, "Sheet1",1);
        String dataEmployes [][] = new String[rownum][colcount];
        for(int i=1;i<=rownum;i++) {
            for(int j=0;j<colcount;j++) {
                dataEmployes[i-1][j] = Utility.getCellData(path,"Sheet1",i,j);
            }
        }
        return(dataEmployes);
    }

    @Test(priority = 3)
    private void loginPasswordLowercase() throws InterruptedException {
        String telephone = loadFile("telephone.txt");
        String password = loadFile("password.txt").toLowerCase();
        for (int i=0;i<2;i++) {
            login(telephone, password);

            //checking response body
            checkBody("Password is invalid.");

            //checking status code
            checkDevStatus("005");
            checkStatusCode("400");

            //checking timestamp
            checkTimestamp();

            //checking path
            checkPath(getPathLogin());

            //checking response time
            checkResponseTime("3000");

            //checking empty data
            checkEmptyData();

            Thread.sleep(5);
        }
    }

    @Test(priority = 4)
    private void loginPasswordUppercase() throws InterruptedException {
        String telephone = loadFile("telephone.txt");
        String password = loadFile("password.txt").toUpperCase();

        login(telephone, password);

        //checking response body
        checkBody("Password is invalid.");

        //checking status code
        checkDevStatus("005");
        checkStatusCode("400");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathLogin());

        //checking response time
        checkResponseTime("3000");

        //checking empty data
        checkEmptyData();

        Thread.sleep(5);
    }

    @Test(priority = 5)
    private void loginPasswordCase1() throws InterruptedException {
        String telephone = loadFile("telephone.txt");
        String password = RestUtils.changePassword1(loadFile("password.txt"));
        for(int i=0;i<2;i++) {

            login(telephone, password);

            //checking response body
            checkBody("Your data do not match our records.");

            //checking status code
            checkDevStatus("011");
            checkStatusCode("404");

            //checking timestamp
            checkTimestamp();

            //checking path
            checkPath(getPathLogin());

            //checking response time
            checkResponseTime("3000");

            //checking empty data
            checkEmptyData();

            Thread.sleep(5);
        }
    }

    @Test(priority = 6)
    private void loginPasswordCase2() throws InterruptedException {
        String telephone = loadFile("telephone.txt");
        String password = RestUtils.changePassword2(loadFile("password.txt"));

        login(telephone, password);

        //checking response body
        checkBody("Your data do not match our records.");

        //checking status code
        checkDevStatus("011");
        checkStatusCode("404");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathLogin());

        //checking response time
        checkResponseTime("3000");

        //checking empty data
        checkEmptyData();

        Thread.sleep(5);
    }
}

package com.danaDeals.test.member.logout;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.RestUtils;
import org.json.JSONException;
import org.testng.annotations.Test;


public class TS_02_Logout_Failed extends TestBase {

    @Test
    private void logoutFailed1() throws JSONException {
        logger.info("***** " + getClass().getName() + " *****");
        String token = loadFile("token.txt");
        token = token.toUpperCase();
        String idUser = loadFile("idUser.txt");
        for(int i=0;i<4;i++) {
            if(i==1){
                token=token.toLowerCase();
            }
            else if(i==2){
                token = loadFile("token.txt")+"0";
            }
            else if(i==3){
                token=loadFile("token.txt");
                idUser=Integer.toString(Integer.parseInt(idUser)-1);
            }
            logout(idUser, token);

            //check response body
            checkBody("You are not authorized.");

            //check status code
            checkDevStatus("021");
            checkStatusCode("401");

            //check response time
            checkResponseTime("3000");

            //checking timestamp
            checkTimestamp();

            //checking path
            checkPath(getPathLogout(idUser));

            //checking empty data
            checkEmptyData();
        }
    }

}

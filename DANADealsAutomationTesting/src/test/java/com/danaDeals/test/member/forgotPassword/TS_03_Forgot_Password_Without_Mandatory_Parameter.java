package com.danaDeals.test.member.forgotPassword;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.RestUtils;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import org.testng.annotations.Test;

public class TS_03_Forgot_Password_Without_Mandatory_Parameter extends TestBase {

    @Test(priority = 0)
    private void forgotPasswordWithoutMandatoryParameter(){
        String password = RestUtils.passwordGenerator();
        String idUser = loadFile("idUser.txt");

        for(int i=0;i<7;i++) {
            RestAssured.baseURI = BaseURI;
            httpRequest = RestAssured.given();

            org.json.simple.JSONObject requestParams = new org.json.simple.JSONObject();
            if(i==0){
                requestParams.put("password", password);
            }
            else if(i==1){
                requestParams.put("confirmPassword", password);
            }
            else if(i==2){
                requestParams.put("password", password);
                requestParams.put("confirmPassword", null);
            }
            else if(i==3){
                requestParams.put("password", null);
                requestParams.put("confirmPassword", password);
            }
            else if(i==4){
                requestParams.put("password", null);
                requestParams.put("confirmPassword", null);
            }
            else if(i==5){
                requestParams.put("password", null);
            }
            else if(i==6){
                requestParams.put("confirmPassword", null);
            }
            httpRequest.header("Content-Type", "application/json");
            httpRequest.body(requestParams.toJSONString());

            response = httpRequest.request(Method.POST, "/api/auth/" + idUser + "/forgot-password");
            responseBody = response.getBody().asString();

            //checking response body
            checkBody("Please fill in all the forms!");

            //checking status code
            checkDevStatus("002");
            checkStatusCode("400");

            //checking response time
            checkResponseTime("3000");

            //checking timestamp
            checkTimestamp();

            //checking empty data
            checkEmptyData();

            //checking path
            checkPath(getPathForgotPassword(idUser));
        }
    }

    @Test(priority = 0)
    private void forgotPasswordWithoutConfirmPassword(){
        String password = RestUtils.passwordGenerator();
        String idUser = loadFile("idUser.txt");
        RestAssured.baseURI = BaseURI;
        httpRequest = RestAssured.given();

        org.json.simple.JSONObject requestParams = new org.json.simple.JSONObject();
        requestParams.put("password", password);

        httpRequest.header("Content-Type", "application/json");
        httpRequest.body(requestParams.toJSONString());

        response = httpRequest.request(Method.POST, "/api/auth/"+idUser+"/forgot-password");
        responseBody = response.getBody().asString();

        //checking response body
        checkBody("Please fill in all the forms!");

        //checking status code
        checkDevStatus("002");
        checkStatusCode("400");

        //checking response time
        checkResponseTime("3000");

        //checking timestamp
        checkTimestamp();

        //checking empty data
        checkEmptyData();

        //checking path
        checkPath(getPathForgotPassword(idUser));

    }
}

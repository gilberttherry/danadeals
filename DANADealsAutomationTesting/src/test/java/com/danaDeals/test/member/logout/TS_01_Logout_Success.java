package com.danaDeals.test.member.logout;

import com.danaDeals.base.TestBase;
import org.json.JSONException;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class TS_01_Logout_Success extends TestBase {

    @Test(priority = 0)
    private void logoutSuccess() throws JSONException {
        logger.info("***** " + getClass().getName() + " *****");
        String token = loadFile("token.txt");
        String idUser = loadFile("idUser.txt");

        logout(idUser,token);

        //check response body
        checkBody("You are logged out.");

        //check status code
        checkStatusCode("200");
        checkDevStatus("041");

        //check response time
        checkResponseTime("3000");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathLogout(idUser));

        //checking empty data
        checkEmptyData();
    }

}

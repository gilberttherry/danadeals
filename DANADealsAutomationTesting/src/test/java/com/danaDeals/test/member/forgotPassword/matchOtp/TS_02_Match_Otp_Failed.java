package com.danaDeals.test.member.forgotPassword.matchOtp;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.Utility;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;

public class TS_02_Match_Otp_Failed extends TestBase {

    @BeforeClass
    private void postRequestOtp() throws InterruptedException{
        logger.info("********" +getClass().getName()+ "*********");
        Thread.sleep(5);
    }

    @Test(dataProvider = "dataOtpNotMatch", priority = 0)
    private void dataDrivenMatchOtpNotMatch(String otp) throws InterruptedException{

        String idUser = loadFile("idUser.txt");
        matchOtp(otp,idUser);

        //checking response body
        checkBody("OTP not match.");

        //checking status code
        checkDevStatus("016");
        checkStatusCode("400");

        //checking response time
        checkResponseTime("3000");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathMatchOtp(idUser));

        //checking empty data
        checkEmptyData();

        Thread.sleep(5);
    }

    @DataProvider(name="dataOtpNotMatch")
    private Object[][] getDataEmptyOtpNotMatch() throws IOException {
        String path = "src/test/java/com/danaDeals/test/member/forgotPassword/matchOtp/dataOtpNotMatch.xlsx";
        int rownum = Utility.getRowCount(path, "Sheet1");
        int colcount = Utility.getCellCount(path, "Sheet1",1);
        String dataEmployes [][] = new String[rownum][colcount];
        for(int i=1;i<=rownum;i++) {
            for(int j=0;j<colcount;j++) {
                dataEmployes[i-1][j] = Utility.getCellData(path,"Sheet1",i,j);
            }
        }
        return(dataEmployes);
    }
}

package com.danaDeals.test.member.forgotPassword.requestOtp;

import com.danaDeals.base.TestBase;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import org.testng.annotations.Test;

public class TS_03_Request_Otp_Without_Mandatory_Parameter extends TestBase {

    @Test(priority = 0)
    private void requestOtpWithoutTelephone(){
        for(int i=0;i<2;i++) {

            RestAssured.baseURI = BaseURI;
            httpRequest = RestAssured.given();

            org.json.simple.JSONObject requestParams = new org.json.simple.JSONObject();
            httpRequest.header("Content-Type", "application/json");
            httpRequest.body(requestParams.toJSONString());
            if(i==1){
                requestParams.put("phoneNumber", null);
            }
            response = httpRequest.request(Method.POST, "/api/auth/request-otp");
            responseBody = response.getBody().asString();

            //checking response body
            checkBody("Please fill in all the forms!");

            //checking status code
            checkDevStatus("002");
            checkStatusCode("400");

            //checking timestamp
            checkTimestamp();

            //checking path
            checkPath(getPathRequestOtp());

            //checking response
            checkEmptyData();

            //checking response time
            checkResponseTime("3000");
        }
    }
}
